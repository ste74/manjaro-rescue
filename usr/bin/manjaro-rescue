#!/bin/bash
##################################
#  Author: Stefano Capitani <www.manjaro.org> <stefanoatmanjarodotorg>
#  Original authors:
#
#  Author1: Bruno Goncalves (www.biglinux.com.br) 
#  Author2: Rafael Ruscher (rruscher@gmail.com)  
#  Date:    2022/08/19
#  
#  Description: Restore GRUB and installed system
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
##################################

#Translation
export TEXTDOMAINDIR="/usr/share/locale"
export TEXTDOMAIN=manjaro-rescue


# only in live mode
if [ ! -e /livefs-pkgs.txt ] ;
then
    zenity --warning --text $"This utility should only be used in live mode!"  --window-icon=/usr/share/manjaro/grub-restore/grub-icon.svg 
    exit
fi

# Enable run pamac from chroot
xhost +

# only first screen
zenity --question \
--text=$"<span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed?" --width=300 --height=100 --title=$"Restore Boot, Grub, Initrd and others stuff" --window-icon=/usr/share/manjaro/grub-restore/grub-icon.svg --icon-name='system-run' --no-wrap

if [ "$?" = "1" ];
then
    exit
fi

# Load info from system
sudo /usr/share/manjaro/grub-restore/grub-restore | zenity --progress --pulsate --no-cancel --auto-close --text $"Wait..."

# No Linux detected
if [ "$(wc -l < /tmp/os-prober)" = "0" ];
then
zenity --error --title=$"Restore Boot, Grub, Initrd and others stuff" --text $"I didn't find any Linux installed on this computer, canceled process.
"  --width=200 --height=25 --window-icon=/usr/share/manjaro/grub-restore/grub-icon.svg
exit
fi

# boot LEGACY without partition LEGACY
if [ ! -d /sys/firmware/efi ] && [ "$(cat /tmp/efi-partitions)" = "" ] ;
then
LIVEBOOT=LEGACY
PARTITION=LEGACY
fi

# boot EFI without partition EFI
if [ -d /sys/firmware/efi ] && [ "$(cat /tmp/efi-partitions)" = "" ] ;
then
LIVEBOOT=EFI
PARTITION=LEGACY
zenity --question --text=$"The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work?" --width=300 --height=100 --title=$"Restore Boot, Grub, Initrd and others stuff" --window-icon=/usr/share/manjaro/grub-restore/grub-icon.svg --icon-name='drive-harddisk' --no-wrap
    if [ "$?" = "1" ];
    then
        exit
    fi
fi


# boot legacy and partition EFI
if [ ! -d /sys/firmware/efi ] && [ "$(cat /tmp/efi-partitions)" != "" ] ;
then
LIVEBOOT=LEGACY
PARTITION=EFI

zenity --question --text=$"Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work?" --width=300 --height=100 --title=$"Restore Boot, Grub, Initrd and others stuff" --window-icon=/usr/share/manjaro/grub-restore/grub-icon.svg --icon-name='drive-harddisk' --no-wrap
    if [ "$?" = "1" ];
    then
        exit
    fi
fi


# boot EFI and partition EFI
if [ -d /sys/firmware/efi ] && [ "$(cat /tmp/efi-partitions)" != "" ] ;
then
LIVEBOOT=EFI
PARTITION=EFI
    if [ "$(wc -l < /tmp/efi-partitions)" = "1" ];
    then
        cp /tmp/efi-partitions /tmp/efi-selected
    fi

    if [ "$(wc -l < /tmp/efi-partitions)" -gt "1" ];
    then

zenity --list --title=$"Restore Boot, Grub, Initrd and others stuff" --width=910 --height=400 --window-icon=/usr/share/manjaro/grub-restore/grub-icon.svg --text $"<span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use:" --hide-header --column="" < /tmp/efi-partitions > /tmp/efi-selected

        if [ "$(cat /tmp/efi-selected)" = "" ];
        then
            zenity --error --text=$"No EFI partition selected, canceled process." --width=300 --height=100 --title=$"Restore the Boot, Grub and Initrd "
            exit
        fi
    fi

fi


# More than one Linux detected
rm -f /tmp/os-prober-selected
if [ "$(wc -l < /tmp/os-prober)" -gt "1" ];
then
zenity --list --title=$"Restore Boot, Grub, Initrd and others stuff" --width=910 --height=400 --window-icon=/usr/share/manjaro/grub-restore/grub-icon.svg  --text $"<span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot:" --hide-header --column="" < /tmp/os-prober > /tmp/os-prober-selected
        if [ "$(cat /tmp/os-prober-selected)" = "" ];
        then
            zenity --error --text=$"No selected partition, canceled process." --width=300 --height=100 --title=$"Restore Boot, Grub, Initrd and others stuff"
            exit
        fi
else
cp -f /tmp/os-prober /tmp/os-prober-selected
fi


# Select MBR and Confirm
if [ "$LIVEBOOT" = "LEGACY" ];
then
    if [ "$(wc -l < /tmp/grub-disks)" -gt "1" ];
    then
    
    zenity --list --title=$"Restore Boot, Grub, Initrd and others stuff" --width=910 --height=400 --window-icon=/usr/share/manjaro/grub-restore/grub-icon.svg --text $"<span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot:" --column=$"Disk" --column=$"Size" --column=$"Name" --column=$"Partition table" $(cat /tmp/grub-disks) > /tmp/grub-disks-selected
    else
        cut -f1 /tmp/grub-disks -d" " > /tmp/grub-disks-selected
    fi

    DISK_SELECTED="$(cat /tmp/grub-disks-selected)"
    DISK_NAME="$(grep "${DISK_SELECTED}" /tmp/grub-disks | cut -f3 -d" ")"
    DISK_TABLE="$(grep "${DISK_SELECTED}" /tmp/grub-disks | cut -f4 -d" ")"
    DISK_SIZE="$(grep "${DISK_SELECTED}" /tmp/grub-disks | cut -f2 -d" ")"
    SELECTED_PARTITION="$(awk -F: '{ print $1}' /tmp/os-prober-selected)"
    SELECTED_OS="$(awk -F: '{ print $2 " - " $3 }' /tmp/os-prober-selected)"
    PARTITION_FORMAT="$(awk -F: '{ print $5 }' /tmp/os-prober-selected)"
    UUID_PARTITION="$(awk -F: '{ print $6 }' /tmp/os-prober-selected | sed 's|UUID=||g')"

    # Confirm
    zenity --question --text=$"<span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed?" --width=300 --height=100 --title=$"Restore Boot, Grub, Initrd and others stuff"  --window-icon=/usr/share/manjaro/grub-restore/grub-icon.svg --icon-name='drive-harddisk' --no-wrap

    if [ "$?" = "1" ];
    then
        exit
    else
        net=$(ping -c 1 8.8.8.8 | cut -d " " -f 1 | grep 64)
        if [ "$net" != "64" ]; then
            zenity --info --text $"Your computer is not connected to the internet, options 2 and 3 need internet to work." --width=300 --height=25 --icon-name='network-offline' --window-icon=/usr/share/manjaro/grub-restore/grub-icon.svg
        fi
        rm -f /tmp/grub-restore-apply-mode
        zenity --list --title=$"Restore Boot, Grub, Initrd and others stuff" --width=910 --height=400 --window-icon=/usr/share/manjaro/grub-restore/grub-icon.svg --text $"<span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options." \
--hide-header \
--column=$"Option" --column=$"Description" \
1 $"Simple, just record the grub again at the beginning of the disk." \
2 $"Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd." \
3 $"Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed." \
4 $"Interactive, opens a terminal within the selected system." \
5 $"Interactive, opens a TUI control center within the selected system." \
6 $"Collect useful logs from installed system." \
7 $"Interactive, opens the package manager within the selected system." \
8 $"Restore System with Timeshift ( only if Timeshift is configurated )" > /tmp/grub-restore-apply-mode
        if [ "$?" = "1" ];
        then
            exit
    
        else
            #restore
            sudo st -e /usr/share/manjaro/grub-restore/grub-apply-legacy
            zenity --info --text $"Done!" --window-icon=/usr/share/manjaro/grub-restore/grub-icon.svg 
        fi
    fi
fi


# EFI Confirm
if [ "$LIVEBOOT" = "EFI" ];
then
    EFI_PARTITION="$(cat /tmp/efi-selected)"
    SELECTED_PARTITION="$(awk -F: '{ print $1}' /tmp/os-prober-selected)"
    SELECTED_OS="$(awk -F: '{ print $2 " - " $3 }' /tmp/os-prober-selected)"
    PARTITION_FORMAT="$(awk -F: '{ print $5 }' /tmp/os-prober-selected)"
    UUID_PARTITION="$(awk -F: '{ print $6 }' /tmp/os-prober-selected | sed 's|UUID=||g')"

    # Confirm
    zenity --question --text=$"<span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed?" --width=300 --height=100 --title=$"Restore Boot, Grub, Initrd and others stuff"  --window-icon=/usr/share/manjaro/grub-restore/grub-icon.svg --icon-name='drive-harddisk' --no-wrap  

    if [ "$?" = "1" ];
    then
        exit
    else
        net=$(ping -c 1 8.8.8.8 | cut -d " " -f 1 | grep 64)
        if [ "$net" != "64" ]; then
            zenity --info --text $"Your computer is not connected to the internet, options 2 and 3 need internet to work." --width=300 --height=25 --icon-name='network-offline' --window-icon=/usr/share/manjaro/grub-restore/grub-icon.svg
        fi
        rm -f /tmp/grub-restore-apply-mode

        
        zenity --list --title=$"Restore Boot, Grub, Initrd and others stuff" --width=910 --height=400 --window-icon=/usr/share/manjaro/grub-restore/grub-icon.svg --text $"<span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options." \
--hide-header \
--column=$"Option" --column=$"Description" \
1 $"Simple, just record the grub again at the beginning of the disk." \
2 $"Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd." \
3 $"Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed." \
4 $"Interactive, opens a terminal within the selected system." \
5 $"Interactive, opens a TUI control center within the selected system." \
6 $"Collect useful logs from installed system." \
7 $"Interactive, opens the package manager within the selected system."
8 $"Restore System with Timeshift ( only if Timeshift is configurated )" > /tmp/grub-restore-apply-mode
        if [ "$?" = "1" ];
        then
            exit
    
        else
            #restore
            sudo st -e /usr/share/manjaro/grub-restore/grub-apply-efi
            zenity --info --text $"Done!" --window-icon=/usr/share/manjaro/grub-restore/grub-icon.svg 
        fi
    fi
fi
