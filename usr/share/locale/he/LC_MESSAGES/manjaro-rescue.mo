��    #      4      L      L  �  M    �  �  �  �   �  �   /	  �   �	  9  l
  9  �  7  �  *     |   C     �     �     �  F   �  C     9   b  B   �  k   �     K  ,   P  (   }     �     �  +   �  C   �  "   -  @   P     �  .  �  .   �     �       V       l  �  t      �  %  {   �  �   '  �   �  _  ^  R  �  �    E   �  �   �  
   c     n     w  b   �  Z   �  Q   C   a   �   }   �      u!  6   z!  4   �!     �!     �!  ;   "  E   C"  +   �"  B   �"     �"    #  <   �$     �$  	   �$  �   �$   <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 כלי זה צריך להיות משמש במצב חי ולשחזר את האתחול של מנג'ארו המותקן על כונן קשיח או SSD.

אם המערכת המותקנת מתחילה נכון אין בעיות באתחול, לכן עדיף לא להמשיך בשימוש בכלי זה.

כנראה שגם יעבוד עם הפצות לינוקס אחרות.

האם ברצונך להמשיך? אשר את הנתונים:

מצב הטעינה: efi
מחיצת efi: $efi_partition

מחיצה נבחרת: $selected_partition
מערכת נבחרת: $selected_os
פורמט המחיצה: $partition_format
מזהה המחיצה: $uuid_partition

האם ברצונך להמשיך? אשר את הנתונים:

מצב ההפעלה: לגסי/ביוס
דיסק שנבחר: $disk_selected

טבלת מחיצות הדיסק: $disk_table

גודל הדיסק: $disk_size

המחיצה שנבחרה: $selected_partition
המערכת שנבחרה: $selected_os
פורמט המחיצה: $partition_format
UUID של המחיצה: $uuid_partition

האם ברצונך להמשיך? בחר אפשרות אחת: נמצאו יותר מפרטיות אחת של EFI, בחר איזו תרצה להשתמש בה: בחר אפשרות מתוך: נמצאו יותר ממערכות ההפעלה של לינוקס על מחשב זה, בחר איזו מהן יש לשחזר את האתחול: בחר אפשרות אחת: נמצאו יותר ממכשיר אחסון אחד, בחר איזה מכשיר יקלוט את ההפעלה. בחר את האפשרות הרצויה:
שיקום ניתן לבצע ב-3 דרכים (פשוטה, ביניים או מלאה), אם יש ספק, נסה את האפשרות הראשונה, הפעל מחדש את המחשב וראה אם הצלחת לפתור את הבעיה.

אם הבעיה ממשיכה, נסה את האפשרויות האחרות. בחר את האפשרות הרצויה: שיקום ניתן לבצע ב-3 דרכים (פשוטה, ביניים או מלאה), אם יש ספק, נסה את האפשרות הראשונה, הפעל מחדש את המחשב וראה אם פתרת את הבעיה.

אם הבעיה ממשיכה, נסה את האפשרויות האחרות. ההפעלה במצב חי נעשית באמצעות מצב מורשת, המכונה גם BIOS, אך מצאתי לפחות מחיצת EFI אחת במחשב זה, כנראה ששחזור ה-Grub לא יעבוד.

נסה לאתחל מחדש ולהפעיל את המחשב במצב EFI דרך ה-BIOS.

האם ברצונך לנסות גם כאשר יש סיכוי קטן שזה יעבוד? צבור יומנים שימושיים מהמערכת המותקנת. השלמה, ביצוע שלבי השיקום האמצעי, עדכון המערכת ובדיקה האם הליבה של lts מותקנת. תיאור דיסק סיימתי! לא מצאתי את הלינוקס מותקן על מחשב זה, ביטלתי את התהליך. אינטראקטיבי, פותח מרכז בקרת TUI בתוך המערכת שנבחרה. אינטראקטיבי, פותח טרמינל בתוך המערכת הנבחרת. אינטראקטיבי, פותח את מנהל החבילות בתוך המערכת הנבחרת. ביניים, מתקין מחדש את חבילת ה- grub במחיצה, יוצר את התצורה ומעדכן את initrd. שם לא נבחרה מחיצת EFI, התהליך בוטל. אין מחיצה נבחרת, התהליך בוטל. אפשרות טבלת מחיצה שחזור הפעלה, grub, initrd ודברים אחרים שחזור מערכת עם timeshift (רק אם timeshift מוגדר) שחזר את האתחול, grub ו-initrd. פשוט, רשום שוב את הגראב בתחילת הדיסק. גודל המצב החי של המערכת משתמש במצב EFI, אך <span foreground='red'>לא מצאתי שום מחיצת EFI</span> במחשב זה, כנראה ששחזור ה-Grub לא יעבוד.

נסה לאתחל מחדש ולשנות את המצב ב-BIOS של המחשב למצב לגסי או BIOS.

האם ברצונך לנסות גם עם הסיכוי הקטן להצליח? כלי זה צריך להיות משמש רק במצב חי! טיימשיפ לא מותקן! חכה... המחשב שלך אינו מחובר לאינטרנט, אפשרויות 2 ו-3 דורשות חיבור לאינטרנט כדי לעבוד. 