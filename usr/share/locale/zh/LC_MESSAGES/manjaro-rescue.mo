��    #      4      L      L  �  M    �  �  �  �   �  �   /	  �   �	  9  l
  9  �  7  �  *     |   C     �     �     �  F   �  C     9   b  B   �  k   �     K  ,   P  (   }     �     �  +   �  C   �  "   -  @   P     �  .  �  .   �     �       V       l    t  �   �  !  l  R   �  o   �  ^   Q  �   �  �   �  5  �  0   �  N   �     L     S  	   Z  J   d  ?   �  -   �  9     L   W     �  $   �  '   �     �  	   �  +   	  9   5  "   o  2   �     �  !  �  0   �           8   P   E    <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 这个工具应该在实时模式下使用，并恢复安装在硬盘或固态硬盘上的Manjaro引导。

如果安装的系统能够正确启动，那么引导没有问题，所以最好不要继续使用这个工具。

可能也适用于其他Linux发行版。

你想继续吗？ 确认数据：

启动模式：efi
efi分区：$efi_partition

选择的分区：$selected_partition
选择的系统：$selected_os
分区格式：$partition_format
分区UUID：$uuid_partition

您是否要继续？ 确认数据：

启动模式：传统/BIOS
选择的磁盘：$disk_selected

磁盘分区表：$disk_table

磁盘大小：$disk_size

选择的分区：$selected_partition
选择的系统：$selected_os
分区格式：$partition_format
分区UUID：$uuid_partition

您是否要继续？ 选择一个选项：
我发现了多个EFI分区，请选择要使用的分区： 选择一个选项：
我发现这台电脑上安装了多个Linux系统，请选择要恢复引导的系统： 选择一个选项：
我发现了多个存储设备，请选择哪一个应该记录启动： 选择所需的选项：

恢复可以通过3种方式进行（简单、中级或完整），如果有疑问，请尝试第一个选项，重新启动计算机，看看是否解决了问题。

如果问题仍然存在，请尝试其他选项。 选择所需的选项：

恢复可以通过3种方式（简单、中级或完整）来完成，如果有疑问，请尝试第一个选项，重新启动计算机并查看是否解决。

如果问题仍然存在，请尝试其他选项。 在实时模式下启动使用传统模式，也称为BIOS，
<span foreground='red'>但我发现这台电脑上至少有一个EFI分区</span>，
可能grub恢复将无法工作。

尝试重新启动，并通过电脑的BIOS将启动模式设置为EFI模式。

你想尝试一下即使可能性很小吗？ 从已安装的系统中收集有用的日志。 完成中间恢复的阶段，更新系统并检查是否安装了lts内核。 描述 磁盘 完成！ 我在这台电脑上没有发现安装有Linux系统，已取消进程。 交互式，打开一个在选定系统内的tui控制中心。 交互式，打开所选系统内的终端。 交互式，打开所选系统中的软件包管理器。 中级，重新安装grub软件包到分区，生成配置并更新initrd。 名字 未选择efi分区，取消进程。 没有选择的分区，取消进程。 选项 分区表 恢复启动，grub，initrd和其他东西 使用timeshift恢复系统（仅当timeshift已配置） 恢复引导程序、grub和initrd 简单，只需在磁盘开头再次记录 grub。 大小 引导模式下的引导程序正在使用efi模式，但是在这台电脑上我没有找到任何efi分区，可能grub恢复将无法工作。

尝试重新启动，并通过电脑的bios将引导模式设置为传统或bios模式。

你想尝试即使可能成功的几率很小吗？ 此实用程序应仅在实时模式下使用！ 时光转移未安装！ 等一下... 你的电脑没有连接到互联网，选项2和3需要互联网才能工作。 