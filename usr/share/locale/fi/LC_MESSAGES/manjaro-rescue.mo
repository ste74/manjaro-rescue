��    #      4      L      L  �  M    �  �  �  �   �  �   /	  �   �	  9  l
  9  �  7  �  *     |   C     �     �     �  F   �  C     9   b  B   �  k   �     K  ,   P  (   }     �     �  +   �  C   �  "   -  @   P     �  .  �  .   �     �       V       l  X  t  �   �  "  �  d   �  �   -  f   �    !    <  �  \  C   �  g   (     �     �     �  V   �  F   �  >   D  C   �  T   �       &   !  %   H  
   n     y  0   �  J   �  $      :   (      c   ^  h   4   �!     �!     "  i   !"   <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 Tämä työkalu tulisi käyttää live-tilassa ja palauttaa Manjaro-käynnistys asennettuna kiintolevylle tai SSD:lle. Jos asennettu järjestelmä käynnistyy oikein, käynnistyksessä ei ole ongelmia, joten on parempi olla jatkamatta tämän työkalun käyttöä. Todennäköisesti toimii myös muiden Linux-jakeluiden kanssa. Haluatko jatkaa? Vahvista tiedot:

Käynnistystila: efi
EFI-osio: $efi_partition

Valittu osio: $selected_partition
Valittu järjestelmä: $selected_os
Osion formaatti: $partition_format
Osion UUID: $uuid_partition

Haluatko jatkaa? Vahvista tiedot:

Käynnistystila:
perintö/bios
Valittu levy:
$disk_selected

Levyn osiotaulukko:
$disk_table

Levyn koko:
$disk_size

Valittu osio:
$selected_partition
Valittu käyttöjärjestelmä:
$selected_os
Osion muoto:
$partition_format
Osion UUID:
$uuid_partition

Haluatko jatkaa? Valitse vaihtoehto:
Löysin useita efi-osioita, valitse käytettäväksi se, jota haluat käyttää: Valitse vaihtoehto:

Löysin useamman kuin yhden Linuxin asennettuna tähän tietokoneeseen, valitse mikä niistä palauttaa käynnistyksen: Valitse vaihtoehto:
Löysin useita tallennuslaitteita, valitse mikä niistä tallentaa käynnistyksen: Valitse haluamasi vaihtoehto:

Palautus voidaan tehdä kolmella tavalla (yksinkertainen, keskitaso tai täydellinen), jos olet epävarma, kokeile ensimmäistä vaihtoehtoa, käynnistä tietokone uudelleen ja katso, ratkesiko ongelma.

Jos ongelma jatkuu, kokeile muita vaihtoehtoja. Valitse haluamasi vaihtoehto:

Palautus voidaan tehdä kolmella tavalla (yksinkertainen, keskitaso tai täydellinen), jos olet epävarma, kokeile ensimmäistä vaihtoehtoa, käynnistä tietokone uudelleen ja katso, onko ongelma ratkaistu.

Jos ongelma jatkuu, kokeile muita vaihtoehtoja. Käynnistäminen live-tilassa käyttää perintötilaa, jota kutsutaan myös biosiksi,
<span foreground='red'>mutta löysin vähintään yhden efi-osion tältä tietokoneelta</span>,
todennäköisesti grubin palautus ei toimi.

Yritä käynnistää uudelleen ja tehdä käynnistys efi-tilassa tietokoneen biosin kautta.

Haluatko kokeilla vaikka onkin epätodennäköistä, että se toimisi? Kerää hyödyllisiä lokitiedostoja asennetusta järjestelmästä. Suorita välivaiheen palautuksen vaiheet, päivitä järjestelmä ja tarkista, onko lts-ydin asennettu. Kuvaus Levyke Valmis! En löytänyt mitään Linuxia asennettuna tähän tietokoneeseen, peruutin prosessin. Interaktiivinen, avaa tui-ohjauskeskuksen valitussa järjestelmässä. Interaktiivinen, avaa terminaalin valitussa järjestelmässä. Interaktiivinen, avaa paketinhallinnan valitussa järjestelmässä. Välitasemeen asentaa grub-paketin osioon, luo kokoonpanon ja päivittää initrd:n. Nimi Ei valittu efi-osio, prosessi peruttu. Ei valittua osiota, prosessi peruttu. Vaihtoehto Osio taulukko Palauta käynnistys, grub, initrd ja muut asiat. Palauta järjestelmä timeshiftillä (vain jos timeshift on konfiguroitu). Palauta käynnistys, grub ja initrd. Yksinkertaista, tallenna vain grub uudelleen levyn alkuun. Koko Käynnistyslevy käyttää efi-tilaa, mutta en löytänyt tältä tietokoneelta <span foreground='red'>yhtään efi-osioita</span>, joten grubin palautus ei todennäköisesti toimi.

Yritä käynnistää uudelleen ja muuttaa tietokoneen bios-asetuksia legacy- tai bios-tilaan.

Haluatko yrittää, vaikka onkin epätodennäköistä, että se toimisi? Tämä työkalu tulisi käyttää vain live-tilassa! Timeshift ei ole asennettu! Odota... Tietokoneesi ei ole yhdistetty internetiin, vaihtoehdot 2 ja 3 tarvitsevat toimiakseen internet-yhteyden. 