��    #      4      L      L  �  M    �  �  �  �   �  �   /	  �   �	  9  l
  9  �  7  �  *     |   C     �     �     �  F   �  C     9   b  B   �  k   �     K  ,   P  (   }     �     �  +   �  C   �  "   -  @   P     �  .  �  .   �     �       V       l  l  t  �   �  +  �  a   �  �   N  v   �    G    _  u    ;   �  p   1     �     �     �  V   �  M     @   Z  J   �  f   �     M  ;   T  ,   �     �     �  0   �  M      !   V   6   x      �   1  �   I   �!     4"  
   S"  q   ^"   <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 Tento nástroj by měl být používán v režimu naživo a pro obnovení Manjaro bootu nainstalovaného na pevném disku nebo SSD. Pokud je nainstalovaný systém spouštěn správně, nejsou žádné problémy s bootem, takže je lepší nepokračovat v používání tohoto nástroje. Pravděpodobně funguje i s jinými distribucemi Linuxu. Chcete pokračovat? Potvrďte data:

Režim spuštění: efi
Efi oddíl: $efi_partition

Vybraný oddíl: $selected_partition
Vybraný systém: $selected_os
Formát oddílu: $partition_format
UUID oddílu: $uuid_partition

Chcete pokračovat? Potvrďte data:

Režim spuštění:

starý/BIOS
Vybraný disk: $disk_selected

Tabulka oddílů disku: $disk_table

Velikost disku: $disk_size

Vybraný oddíl: $selected_partition
Vybraný systém: $selected_os
Formát oddílu: $partition_format
UUID oddílu: $uuid_partition

Chcete pokračovat? Vyberte možnost:

Našel jsem více než jednu EFI oddíl, vyberte, který by měl být použit: Vyberte možnost:
Na tomto počítači jsem našel více než jednu instalaci Linuxu, vyberte, která by měla obnovit zavaděč: Vyberte možnost:
Našel jsem více než jeden úložný zařízení, vyberte, které by mělo zaznamenat spuštění. Vyberte požadovanou možnost: obnovení lze provést 3 způsoby (jednoduchý, střední nebo úplný), v případě pochybností zkuste první možnost, restartujte počítač a zkontrolujte, zda jste problém vyřešili.

Pokud problém přetrvává, zkuste ostatní možnosti. Vyberte požadovanou možnost:
Obnovení může být provedeno 3 způsoby (jednoduchý, střední nebo úplný), pokud máte pochybnosti, zkuste první možnost, restartujte počítač a zkontrolujte, zda jste vyřešili problém.

Pokud problém přetrvává, zkuste ostatní možnosti. Spouštění v režimu live používá legacy režim, také nazývaný bios,
<span foreground='red'>ale na tomto počítači jsem našel alespoň jednu efi oddíl</span>,
pravděpodobně obnovení grubu nebude fungovat.

zkuste restartovat a přes bios počítače nastavit spouštění v efi režimu.

chcete to zkusit i přesto, že je malá šance, že to bude fungovat? Sbírejte užitečné záznamy z nainstalovaného systému. Dokončete, provede etapy střední obnovy, aktualizuje systém a zkontroluje, zda je nainstalováno lts jádro. Popis Disk Hotovo! Nenašel jsem žádný nainstalovaný linux na tomto počítači, zrušil jsem proces. Interaktivní, otevírá tui ovládací centrum v rámci vybraného systému. Interaktivní, otevírá terminál v rámci vybraného systému. Interaktivní, otevírá správce balíčků v rámci vybraného systému. Prostředník znovu nainstaluje balíček grub v oddílu, vygeneruje konfiguraci a aktualizuje initrd. Jméno Nebyla vybrána žádná efi partition, proces byl zrušen. Žádná vybraná oddílu, zrušený proces. Možnost Tabulka oddílů Obnovit zavaděč, grub, initrd a další věci. Obnovte systém pomocí timeshift (pouze pokud je timeshift nakonfigurován). Obnovit zavaděč, grub a initrd. Jednoduše zaznamenejte grub znovu na začátek disku. Velikost Bota v režimu live používá efi režim, ale na tomto počítači jsem nenašel žádnou efi oddíl, pravděpodobně nebude fungovat obnovení grubu.

Zkuste restartovat a v biosu počítače nastavit boot v legacy nebo bios režimu.

Chcete to zkusit i přesto, že je malá pravděpodobnost úspěchu? Tento nástroj by měl být používán pouze v režimu živého provozu! Timeshift není nainstalován! Počkej... Váš počítač není připojen k internetu, možnosti 2 a 3 vyžadují pro svou funkci připojení k internetu. 