��    #      4      L      L  �  M    �  �  �  �   �  �   /	  �   �	  9  l
  9  �  7  �  *     |   C     �     �     �  F   �  C     9   b  B   �  k   �     K  ,   P  (   }     �     �  +   �  C   �  "   -  @   P     �  .  �  .   �     �       V       l  f  t  �   �  V  �  h   ,  �   �  v   "    �    �  s  �  2     s   O     �     �     �  T   �  Q   1  C   �  O   �  f        ~  7   �  3   �     �     �  .      F   ?   "   �   ;   �   
   �   n  �   A   _"     �"  
   �"  f   �"   <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 Questo strumento dovrebbe essere utilizzato in modalità live e per ripristinare il boot di Manjaro installato su un disco rigido o un SSD. Se il sistema installato si avvia correttamente, non ci sono problemi nel boot, quindi è meglio non continuare a utilizzare questo strumento. Probabilmente funziona anche con altre distribuzioni Linux. Vuoi procedere? Conferma i dati:
Modalità di avvio: efi
Partizione efi: $efi_partition
Partizione selezionata: $selected_partition
Sistema selezionato: $selected_os
Formato della partizione: $partition_format
UUID della partizione: $uuid_partition

Vuoi procedere? Conferma i dati:

Modalità di avvio: legacy/bios
Disco selezionato: $disk_selected
Tabella delle partizioni del disco: $disk_table
Dimensione del disco: $disk_size
Partizione selezionata: $selected_partition
Sistema selezionato: $selected_os
Formato della partizione: $partition_format
UUID della partizione: $uuid_partition

Vuoi procedere? seleziona un'opzione: ho trovato più di una partizione EFI, seleziona quale dovrebbe essere utilizzata: Seleziona un'opzione: Ho trovato più di una versione di Linux installata su questo computer, seleziona quale dovrebbe ripristinare il boot: Seleziona un'opzione: ho trovato più di un dispositivo di archiviazione, seleziona quale dovrebbe registrare l'avvio: Seleziona l'opzione desiderata:
Il ripristino può essere effettuato in 3 modi (semplice, intermedio o completo), se hai dei dubbi, prova la prima opzione, riavvia il computer e verifica se hai risolto il problema.

Se il problema persiste, prova le altre opzioni. Seleziona l'opzione desiderata:

Il ripristino può essere effettuato in 3 modi (semplice, intermedio o completo), se hai dubbi, prova la prima opzione, riavvia il computer e verifica se hai risolto il problema.

Se il problema persiste, prova le altre opzioni. Il boot in modalità live utilizza la modalità legacy, anche chiamata bios,
<span foreground='red'>ma ho trovato almeno una partizione efi su questo computer</span>,
probabilmente il ripristino di grub non funzionerà.

Prova a riavviare e tramite il bios del computer effettua il boot in modalità efi.

Vuoi provare anche se c'è una piccola possibilità che funzioni? Raccogliere registri utili dal sistema installato. Completa, esegue le fasi del ripristino intermedio, aggiorna il sistema e controlla se il kernel lts è installato. Descrizione Disco Fatto! Non ho trovato alcuna installazione di Linux su questo computer, processo annullato. Interattivo, apre un centro di controllo tui all'interno del sistema selezionato. Interattivo, apre un terminale all'interno del sistema selezionato. Interattivo, apre il gestore dei pacchetti all'interno del sistema selezionato. Intermedio, reinstalla il pacchetto grub nella partizione, genera la configurazione e aggiorna initrd. Nome Nessuna partizione EFI selezionata, processo annullato. Nessuna partizione selezionata, processo annullato. Opzione Tabella delle partizioni Ripristina il boot, grub, initrd e altri file. Ripristina il sistema con timeshift (solo se timeshift è configurato) Ripristina il boot, grub e initrd. Semplice, registra nuovamente il grub all'inizio del disco. Dimensione Il boot in modalità live sta utilizzando la modalità efi, ma <span foreground='red'>non ho trovato alcuna partizione efi</span> su questo computer, probabilmente il ripristino di grub non funzionerà.

Prova a riavviare e attraverso il bios del computer imposta il boot in modalità legacy o bios.

Vuoi provare anche se c'è una piccola possibilità che funzioni? Questa utility dovrebbe essere utilizzata solo in modalità live! Timeshift non è installato! Aspetta... Il tuo computer non è connesso a internet, le opzioni 2 e 3 hanno bisogno di internet per funzionare. 