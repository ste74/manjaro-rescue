��    #      4      L      L  �  M    �  �  �  �   �  �   /	  �   �	  9  l
  9  �  7  �  *     |   C     �     �     �  F   �  C     9   b  B   �  k   �     K  ,   P  (   }     �     �  +   �  C   �  "   -  @   P     �  .  �  .   �     �       V       l  r  t    �  �  �  ]   �  �     r   �      6  (  ^  _  <   �  u   �     q   t  w      �!  U   �!  @   J"  5   �"  =   �"  c   �"     c#  <   h#  ,   �#  	   �#     �#  '   �#  J   $     a$  6   $  	   �$  -  �$  @   �%  !   /&  
   Q&  b   \&   <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 Tento nástroj by mal byť použitý v režime naživo a na obnovenie Manjaro bootu nainštalovaného na pevný disk alebo SSD.

Ak sa nainštalovaný systém spúšťa správne, nie sú žiadne problémy s bootovaním, preto je lepšie nepokračovať v používaní tohto nástroja.

Pravdepodobne bude fungovať aj s inými distribúciami Linuxu.

Chcete pokračovať? <span font='16' foreground='lightskyblue'>potvrďte údaje:</span>

<span font='11' foreground='gray'>režim spustenia:</span> efi
<span font='11' foreground='gray'>efi oddiel:</span> $efi_partition

<span font='11' foreground='gray'>vybraný oddiel:</span> $selected_partition
<span font='11' foreground='gray'>vybraný systém:</span> $selected_os
<span font='11' foreground='gray'>formát oddielu:</span> $partition_format
<span font='11' foreground='gray'>uuid oddielu:</span> $uuid_partition

chcete pokračovať? <span font='16' foreground='lightskyblue'>potvrďte údaje:</span>    

<span font='11' foreground='gray'>režim spustenia:</span>  legacy/bios
<span font='11' foreground='gray'>vybratý disk:</span> $disk_selected
    
<span font='11' foreground='gray'>tabuľka diskových oddielov:</span> $disk_table
    
<span font='11' foreground='gray'>veľkosť disku:</span> $disk_size
   
<span font='11' foreground='gray'>vybratý oddiel:</span> $selected_partition
<span font='11' foreground='gray'>vybratý systém:</span> $selected_os
<span font='11' foreground='gray'>formát oddielu:</span> $partition_format
<span font='11' foreground='gray'>uuid oddielu:</span> $uuid_partition

chcete pokračovať? vyberte možnosť:
našiel som viac ako jednu efi partíciu, vyberte, ktorú chcete použiť: Vyberte možnosť: 

Na tomto počítači som našiel viac ako jednu inštaláciu Linuxu, vyberte, ktorú chcete obnoviť pre spustenie systému. Vyberte možnosť: Našiel som viac ako jeden úložný zariadenie, vyberte, ktoré by malo zaznamenať spustenie. Vyberte požadovanú možnosť:

Obnovenie môže byť vykonané 3 spôsobmi (jednoduchým, stredným alebo úplným), ak máte pochybnosti, skúste prvú možnosť, reštartujte počítač a pozrite, či ste vyriešili problém.

Ak problém pretrváva, skúste ostatné možnosti. Vyberte požadovanú možnosť:

Obnovenie môže byť vykonané 3 spôsobmi (<span foreground='gray'>jednoduchý, stredný alebo úplný</span>), ak máte pochybnosti, skúste prvú možnosť, reštartujte počítač a pozrite, či ste vyriešili problém.

Ak problém pretrváva, skúste ostatné možnosti. Spustenie v živom režime používa legacy režim, tiež nazývaný bios,
<span foreground='red'>ale na tomto počítači som našiel aspoň jednu efi partíciu</span>,
pravdepodobne obnovenie grubu nebude fungovať.

Skúste reštartovať a cez bios počítača spustiť režim efi.

Chcete to skúsiť aj napriek malému pravdepodobnosti úspechu? Zbierajte užitočné záznamy z nainštalovaného systému. Dokončí, vykonáva fázy stredného obnovenia, aktualizuje systém a skontroluje, či je nainštalované lts jadro. Popis Disk

Disk je typ úložného média, ktoré sa používa na ukladanie dát. Môže byť vo forme pevného disku alebo optického disku, ako napríklad CD alebo DVD. Disky majú obvykle veľkú kapacitu a sú schopné uchovávať veľké množstvo informácií. Sú nevyhnutným nástrojom pre ukladanie a prenos dát v počítačoch a iných elektronických zariadeniach. Hotovo! Nenašiel som žiadny nainštalovaný linux na tomto počítači, zrušil som proces. Interaktívne, otvára tui ovládací panel v zvolenom systéme. Interaktívne, otvára terminál v zvolenom systéme. Interaktívne, otvára správcu balíkov v zvolenom systéme. Stredný, znovu nainštaluje balík grub do oddielu, vygeneruje konfiguráciu a aktualizuje initrd. Meno Nebola vybraná žiadna efi partícia, proces bol zrušený. Žiadna vybraná particia, proces zrušený. Možnosť Tabuľka oddielov Obnoviť boot, grub, initrd a iné veci Obnoviť systém pomocou timeshift (iba ak je timeshift nakonfigurovaný). Obnoviť boot, grub a initrd. Jednoducho zaznamenajte grub znova na začiatku disku. Veľkosť Bota v živom režime používa efi režim, ale na tomto počítači som nenašiel žiadnu efi partíciu,
pravdepodobne obnovenie grubu nebude fungovať.

Skúste reštartovať a cez bios počítača nastaviť boot v legacy alebo bios režime.

Chcete to skúsiť aj napriek malému šanci na úspech? Táto pomôcka by mala byť používaná iba v reálnom režime! Timeshift nie je nainštalovaný! Počkaj... Váš počítač nie je pripojený k internetu, možnosti 2 a 3 potrebujú internet na fungovanie. 