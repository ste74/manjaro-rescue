��    #      4      L      L  �  M    �  �  �  �   �  �   /	  �   �	  9  l
  9  �  7  �  *     |   C     �     �     �  F   �  C     9   b  B   �  k   �     K  ,   P  (   }     �     �  +   �  C   �  "   -  @   P     �  .  �  .   �     �       V       l     t  �  �    !  �   6  6  �  �   .  c  �  T  b   �  �"  x   �%  �   -&     '     0'     ='  �   I'  �   �'  u   x(  �   �(  �   �)  
   6*  f   A*  b   �*     +  %   +  Y   @+  |   �+  5   ,  a   M,     �,  �  �,  �   {/  <   0     P0  �   d0   <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 Αυτό το εργαλείο πρέπει να χρησιμοποιηθεί σε λειτουργία ζωντανής εκτέλεσης και για την επαναφορά του εκκίνησης του Manjaro που έχει εγκατασταθεί στον σκληρό δίσκο ή τον SSD.

<span font='12' foreground='red'>Εάν το εγκατεστημένο σύστημα ξεκινά σωστά, δεν υπάρχουν προβλήματα με την εκκίνηση, οπότε είναι καλύτερο να μην συνεχίσετε τη χρήση αυτού του εργαλείου.</span>
<span font='10' foreground='gray'>Πιθανώς να λειτουργεί και με άλλες διανομές Linux.</span>

Θέλετε να συνεχίσετε; Επιβεβαιώστε τα δεδομένα:

Λειτουργία εκκίνησης: efi
Διαμέρισμα efi: $efi_partition

Επιλεγμένο διαμέρισμα: $selected_partition
Επιλεγμένο σύστημα: $selected_os
Μορφή διαμέρισματος: $partition_format
UUID διαμέρισματος: $uuid_partition

Θέλετε να συνεχίσετε; Επιβεβαίωση δεδομένων:

Λειτουργία εκκίνησης: παραδοσιακή/BIOS
Επιλεγμένος δίσκος: $disk_selected
Πίνακας διαμερισμάτων δίσκου: $disk_table
Μέγεθος δίσκου: $disk_size
Επιλεγμένο διαμέρισμα: $selected_partition
Επιλεγμένο σύστημα: $selected_os
Μορφή διαμέρισματος: $partition_format
UUID διαμέρισματος: $uuid_partition

Θέλετε να συνεχίσετε; επιλέξτε μια επιλογή: 

βρήκα περισσότερες από μία εφαρμογή efi, επιλέξτε ποια θα πρέπει να χρησιμοποιηθεί: <span font='16' foreground='lightskyblue'>επιλέξτε μια επιλογή:</span>

βρήκα περισσότερα από ένα linux εγκατεστημένα σε αυτόν τον υπολογιστή, επιλέξτε ποιο θα πρέπει να επαναφέρει την εκκίνηση: Επιλέξτε μια επιλογή: Βρήκα περισσότερες από μία συσκευές αποθήκευσης, επιλέξτε ποια θα καταγράψει την εκκίνηση. <span font='16' foreground='lightskyblue'>επιλέξτε την επιθυμητή επιλογή:</span>

η αποκατάσταση μπορεί να γίνει με 3 τρόπους (<span foreground='gray'>απλός, ενδιάμεσος ή πλήρης</span>), αν έχετε αμφιβολίες, δοκιμάστε την πρώτη επιλογή, επανεκκινήστε τον υπολογιστή και δείτε αν έχετε λύσει το πρόβλημα.

αν το πρόβλημα εξακολουθεί να υπάρχει, δοκιμάστε τις άλλες επιλογές. <span font='16' foreground='lightskyblue'>επιλέξτε την επιθυμητή επιλογή:</span>

η επαναφορά μπορεί να γίνει με 3 τρόπους (<span foreground='gray'>απλός, ενδιάμεσος ή πλήρης</span>), αν έχετε αμφιβολίες, δοκιμάστε την πρώτη επιλογή, επανεκκινήστε τον υπολογιστή και δείτε αν λύθηκε το πρόβλημα.

αν το πρόβλημα εξακολουθεί να υπάρχει, δοκιμάστε τις άλλες επιλογές. Η εκκίνηση σε λειτουργία ζωντανού συστήματος γίνεται με τη χρήση της κλασικής λειτουργίας, επίσης γνωστής ως bios, <span foreground='red'>αλλά βρήκα τουλάχιστον μία εφαρμογή efi σε αυτόν τον υπολογιστή</span>, πιθανώς η επαναφορά του grub δεν θα λειτουργήσει.

Προσπαθήστε να επανεκκινήσετε και μέσω του bios του υπολογιστή να κάνετε εκκίνηση σε λειτουργία efi.

Θέλετε να προσπαθήσετε ακόμα και αν υπάρχει μικρή πιθανότητα να λειτουργήσει; Συλλέξτε χρήσιμα αρχεία καταγραφής από το εγκατεστημένο σύστημα. Ολοκληρώνει, εκτελεί τα στάδια της ενδιάμεσης αποκατάστασης, ενημερώνει το σύστημα και ελέγχει αν έχει εγκατασταθεί το lts πυρήνας. Περιγραφή Δίσκος Έγινε! Δεν βρήκα καμία εγκατεστημένη έκδοση Linux σε αυτόν τον υπολογιστή, ακύρωσα τη διαδικασία. Διαδραστικό, ανοίγει ένα κέντρο ελέγχου tui εντός του επιλεγμένου συστήματος. Διαδραστικό, ανοίγει ένα τερματικό μέσα στο επιλεγμένο σύστημα. Διαδραστικό, ανοίγει τον διαχειριστή πακέτων εντός του επιλεγμένου συστήματος. Μεσαίο, επανεγκαθιστά το πακέτο grub στο διαμέρισμα, δημιουργεί τη διαμόρφωση και ενημερώνει το initrd. Όνομα Δεν έχει επιλεγεί διαμέρισμα efi, ακυρώθηκε η διαδικασία. Δεν έχει επιλεγεί διαμέρισμα, ακυρώθηκε η διαδικασία. Επιλογή Πίνακας διαμερισμού Επαναφορά εκκίνησης, grub, initrd και άλλων στοιχείων. Επαναφορά συστήματος με το timeshift (μόνο εάν έχει διαμορφωθεί το timeshift) Επαναφέρετε το boot, grub και initrd. Απλό, απλά καταγράψτε ξανά το grub στην αρχή του δίσκου. Μέγεθος Το boot σε λειτουργία ζωντανής εκκίνησης χρησιμοποιεί τη λειτουργία efi, αλλά <span foreground='red'>δεν βρήκα καμία efi κατάτμηση</span> σε αυτόν τον υπολογιστή, πιθανώς η ανάκτηση του grub δεν θα λειτουργήσει.

Προσπαθήστε να επανεκκινήσετε και μέσω του bios του υπολογιστή να κάνετε το boot σε λεγόμενη λειτουργία κλασικής ή bios.

Θέλετε να δοκιμάσετε ακόμα και αν υπάρχει μικρή πιθανότητα να λειτουργήσει; Αυτό το εργαλείο πρέπει να χρησιμοποιείται μόνο σε λειτουργία ζωντανής εκτέλεσης! Το Timeshift δεν είναι εγκατεστημένο! Περίμενε... Ο υπολογιστής σας δεν είναι συνδεδεμένος στο διαδίκτυο, οι επιλογές 2 και 3 χρειάζονται διαδίκτυο για να λειτουργήσουν. 