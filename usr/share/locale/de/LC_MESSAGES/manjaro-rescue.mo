��    #      4      L      L  �  M    �  �  �  �   �  �   /	  �   �	  9  l
  9  �  7  �  *     |   C     �     �     �  F   �  C     9   b  B   �  k   �     K  ,   P  (   }     �     �  +   �  C   �  "   -  @   P     �  .  �  .   �     �       V       l  �  t  �   I  G  <  q   �  �   �  �   }  9    v  ?  �  �  6   O  �   �       
   *  	   5  F   ?  P   �  E   �  I      t   g      �   5   �   2   !     J!     Q!  :   c!  K   �!  ,   �!  I   "     a"  Z  i"  @   �#     $     "$  |   +$   <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 Dieses Tool sollte im Live-Modus verwendet werden und um den Manjaro-Boot wiederherzustellen, der auf der HD oder SSD installiert ist.

<span font='12' foreground='red'>Wenn das installierte System ordnungsgemäß startet, gibt es keine Probleme beim Booten, daher ist es besser, die Verwendung dieses Tools nicht fortzusetzen.</span>
<span font='10' foreground='gray'>Funktioniert wahrscheinlich auch mit anderen Linux-Distributionen.</span>

Möchten Sie fortfahren? Bestätige die Daten:

Boot-Modus: efi
EFI-Partition: $efi_partition

Ausgewählte Partition: $selected_partition
Ausgewähltes System: $selected_os
Partition-Format: $partition_format
Partition UUID: $uuid_partition

Möchtest du fortfahren? Bestätigen Sie die Daten:

Boot-Modus: Legacy/BIOS
Ausgewählte Festplatte: $disk_selected
Festplattentabelle: $disk_table
Festplattengröße: $disk_size
Ausgewählte Partition: $selected_partition
Ausgewähltes System: $selected_os
Partition Format: $partition_format
Partition UUID: $uuid_partition

Möchten Sie fortfahren? Wähle eine Option aus:

Ich habe mehr als eine EFI-Partition gefunden, wähle aus, welche verwendet werden soll: Wähle eine Option aus:

Ich habe mehr als ein Linux auf diesem Computer gefunden, wähle aus, welches das Boot wiederherstellen soll: Wähle eine Option aus:

Ich habe mehr als ein Speichergerät gefunden, wähle aus, welches für den Bootvorgang verwendet werden soll: Wähle die gewünschte Option aus:

Die Wiederherstellung kann auf 3 Arten durchgeführt werden (einfach, mittel oder vollständig). Wenn du unsicher bist, versuche die erste Option, starte den Computer neu und schau, ob das Problem gelöst ist.

Wenn das Problem weiterhin besteht, versuche die anderen Optionen. Wählen Sie die gewünschte Option aus:

Die Wiederherstellung kann auf 3 Arten durchgeführt werden (<span foreground='gray'>einfach, mittel oder komplett</span>). Wenn Sie sich unsicher sind, versuchen Sie die erste Option, starten Sie den Computer neu und prüfen Sie, ob das Problem behoben wurde.

Wenn das Problem weiterhin besteht, versuchen Sie die anderen Optionen. Der Boot im Live-Modus verwendet den Legacy-Modus, auch BIOS genannt, <span foreground='red'>aber ich habe mindestens eine EFI-Partition auf diesem Computer gefunden</span>. Wahrscheinlich wird die Wiederherstellung von Grub nicht funktionieren.

Versuche, den Computer neu zu starten und über das BIOS des Computers den Boot im EFI-Modus zu machen.

Möchtest du es trotz geringer Erfolgschancen versuchen? Sammle nützliche Protokolle von installiertem System. Vervollständige, führt die Schritte der Zwischenwiederherstellung durch, aktualisiert das System und überprüft, ob der LTS-Kernel installiert ist. Beschreibung Festplatte Erledigt! Ich habe kein Linux auf diesem Computer gefunden, Prozess abgebrochen. Interaktiv, öffnet ein TUI-Kontrollzentrum innerhalb des ausgewählten Systems. Interaktiv, öffnet ein Terminal innerhalb des ausgewählten Systems. Interaktiv, öffnet den Paketmanager innerhalb des ausgewählten Systems. Zwischenstufe, installiert das Grub-Paket in der Partition neu, generiert die Konfiguration und aktualisiert initrd. Name Keine EFI-Partition ausgewählt, Vorgang abgebrochen. Keine ausgewählte Partition, Vorgang abgebrochen. Option Partitionstabelle Stelle den Boot, Grub, Initrd und andere Dinge wieder her. System mit Timeshift wiederherstellen (nur wenn Timeshift konfiguriert ist) Stelle den Boot, Grub und Initrd wieder her. Einfach, nehmen Sie einfach den Grub am Anfang der Festplatte erneut auf. Größe Der Boot im Live-Modus verwendet den EFI-Modus, aber ich habe keine EFI-Partition auf diesem Computer gefunden. Wahrscheinlich wird die Wiederherstellung von Grub nicht funktionieren.

Versuche, den Computer neu zu starten und über das BIOS den Boot im Legacy- oder BIOS-Modus zu machen.

Möchtest du es trotz geringer Erfolgschancen versuchen? Dieses Dienstprogramm sollte nur im Live-Modus verwendet werden! Timeshift nicht installiert! Warte... Ihr Computer ist nicht mit dem Internet verbunden, Optionen 2 und 3 benötigen eine Internetverbindung, um zu funktionieren. 