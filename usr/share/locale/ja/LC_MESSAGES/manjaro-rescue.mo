Þ    #      4      L      L    M    ç  ®  ú     ©  ¥   /	     Õ	  9  l
  9  ¦  7  à  *     |   C     À     Ì     Ñ  F   ×  C     9   b  B     k   ß     K  ,   P  (   }     ¦     ­  +   ½  C   é  "   -  @   P       .    .   Å     ô       V       l  Ü  t  g  Q  Ô  ¹       ß   -  ¯     Þ  ½  Û    Ö  x   N   O"     "     >#     E#     R#  §   h#  l   $  ]   }$  o   Û$     K%     Ô%  `   Û%  i   <&     ¦&  !   ¶&  ?   Ø&  u   '  1   '  Z   À'  	   (  ö  %(  ]   *  ?   z*     º*     Ó*   <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 ãã®ãã¼ã«ã¯ã©ã¤ãã¢ã¼ãã§ä½¿ç¨ããHDã¾ãã¯SSDã«ã¤ã³ã¹ãã¼ã«ãããManjaroãã¼ããå¾©åããããã«ä½¿ç¨ããå¿è¦ãããã¾ãã

ã¤ã³ã¹ãã¼ã«ãããã·ã¹ãã ãæ­£å¸¸ã«èµ·åãã¦ããå ´åããã¼ãã«åé¡ã¯ããã¾ããã®ã§ããã®ãã¼ã«ã®ä½¿ç¨ãç¶ããªãæ¹ãè¯ãã§ãããã

ããããä»ã®Linuxãã£ã¹ããªãã¥ã¼ã·ã§ã³ã§ãåä½ããã§ãããã

ç¶è¡ãã¾ããï¼ <span font='16' foreground='lightskyblue'>ãã¼ã¿ãç¢ºèªãã¾ã:</span>

<span font='11' foreground='gray'>ãã¼ãã¢ã¼ã:</span> efi
<span font='11' foreground='gray'>efiãã¼ãã£ã·ã§ã³:</span> $efi_partition

<span font='11' foreground='gray'>é¸æãããã¼ãã£ã·ã§ã³:</span> $selected_partition
<span font='11' foreground='gray'>é¸æããã·ã¹ãã :</span> $selected_os
<span font='11' foreground='gray'>ãã¼ãã£ã·ã§ã³ã®ãã©ã¼ããã:</span> $partition_format
<span font='11' foreground='gray'>ãã¼ãã£ã·ã§ã³ã®UUID:</span> $uuid_partition

ç¶è¡ãã¾ããï¼ ãã¼ã¿ãç¢ºèªãã¦ãã ããï¼

ãã¼ãã¢ã¼ãï¼
ã¬ã¬ã·ã¼/BIOS
é¸æããããã£ã¹ã¯ï¼
$disk_selected
ãã£ã¹ã¯ã®ãã¼ãã£ã·ã§ã³ãã¼ãã«ï¼
$disk_table
ãã£ã¹ã¯ã®ãµã¤ãºï¼
$disk_size
é¸æããããã¼ãã£ã·ã§ã³ï¼
$selected_partition
é¸æãããã·ã¹ãã ï¼
$selected_os
ãã¼ãã£ã·ã§ã³ã®ãã©ã¼ãããï¼
$partition_format
ãã¼ãã£ã·ã§ã³ã®UUIDï¼
$uuid_partition

ç¶è¡ãã¾ããï¼ ãªãã·ã§ã³ãé¸æãã¦ãã ããï¼

è¤æ°ã®EFIãã¼ãã£ã·ã§ã³ãè¦ã¤ããã¾ãããã©ããä½¿ç¨ãããé¸æãã¦ãã ããã ãªãã·ã§ã³ãé¸æãã¦ãã ããï¼

ãã®ã³ã³ãã¥ã¼ã¿ã¼ã«ã¯è¤æ°ã®Linuxãã¤ã³ã¹ãã¼ã«ããã¦ãããã¨ããããã¾ããããã¼ããå¾©åããããã«ã©ããé¸æãã¾ããï¼ ãªãã·ã§ã³ãé¸æãã¦ãã ããï¼
è¤æ°ã®ã¹ãã¬ã¼ã¸ããã¤ã¹ãè¦ã¤ããã¾ããããã¼ããè¨é²ããããã¤ã¹ãé¸æãã¦ãã ããã <span font='16' foreground='lightskyblue'>å¸æãããªãã·ã§ã³ãé¸æãã¦ãã ããï¼</span>

å¾©åã¯3ã¤ã®æ¹æ³ã§è¡ããã¨ãã§ãã¾ãï¼<span foreground='gray'>ç°¡åãä¸­ç´ãå®å¨</span>ï¼ãçããããå ´åã¯ãæåã®ãªãã·ã§ã³ãè©¦ãã¦ãã³ã³ãã¥ã¼ã¿ã¼ãåèµ·åããåé¡ãè§£æ±ºãããã©ãããç¢ºèªãã¦ãã ããã

åé¡ãè§£æ±ºããªãå ´åã¯ãä»ã®ãªãã·ã§ã³ãè©¦ãã¦ãã ããã <span font='16' foreground='lightskyblue'>å¸æãããªãã·ã§ã³ãé¸æãã¦ãã ããï¼</span>

å¾©åã¯3ã¤ã®æ¹æ³ã§è¡ããã¨ãã§ãã¾ãï¼<span foreground='gray'>ç°¡åãä¸­ç´ãå®å¨</span>ï¼ãçããããå ´åã¯ãæåã®ãªãã·ã§ã³ãè©¦ããã³ã³ãã¥ã¼ã¿ã¼ãåèµ·åããåé¡ãè§£æ±ºãããã©ãããç¢ºèªãã¦ãã ããã

åé¡ãè§£æ±ºããªãå ´åã¯ãä»ã®ãªãã·ã§ã³ãè©¦ãã¦ãã ããã ã©ã¤ãã¢ã¼ãã§ã®ãã¼ãã¯ã¬ã¬ã·ã¼ã¢ã¼ããä½¿ç¨ãã¦ãã¾ããããã¯BIOSã¨ãå¼ã°ãã¾ããããã®ã³ã³ãã¥ã¼ã¿ã¼ã«ã¯å°ãªãã¨ã1ã¤ã®EFIãã¼ãã£ã·ã§ã³ãè¦ã¤ããã¾ããããããããGrubã®å¾©åã¯ãã¾ããããªãã§ãããã

ã³ã³ãã¥ã¼ã¿ã¼ã®BIOSãä»ãã¦EFIã¢ã¼ãã§ã®ãã¼ããè©¦ã¿ã¦ã¿ã¦ãã ããã

ãã¾ãããå¯è½æ§ã¯ä½ãã§ãããè©¦ãã¦ã¿ã¾ããï¼ ã¤ã³ã¹ãã¼ã«ãããã·ã¹ãã ããæç¨ãªã­ã°ãåéããã å®äºããä¸­éä¿®å¾©ã®æ®µéãå®è¡ããã·ã¹ãã ãæ´æ°ããltsã«ã¼ãã«ãã¤ã³ã¹ãã¼ã«ããã¦ãããã©ãããç¢ºèªãã¾ãã èª¬æ ãã£ã¹ã¯ å®äºãã¾ããï¼ ãã®ã³ã³ãã¥ã¼ã¿ã¼ã«ã¯Linuxãã¤ã³ã¹ãã¼ã«ããã¦ããªããã¨ããããã¾ããã§ããã®ã§ããã­ã»ã¹ãã­ã£ã³ã»ã«ãã¾ããã ã¤ã³ã¿ã©ã¯ãã£ããªtuiã³ã³ãã­ã¼ã«ã»ã³ã¿ã¼ãé¸æããã·ã¹ãã åã«éãã¾ãã ã¤ã³ã¿ã©ã¯ãã£ããé¸æãããã·ã¹ãã åã§ã¿ã¼ããã«ãéãã¾ãã ã¤ã³ã¿ã©ã¯ãã£ãã«é¸æãããã·ã¹ãã åã®ããã±ã¼ã¸ããã¼ã¸ã£ã¼ãéãã¾ãã ä¸­ç´èã¯ããã¼ãã£ã·ã§ã³ã«grubããã±ã¼ã¸ãåã¤ã³ã¹ãã¼ã«ããè¨­å®ãçæããinitrdãæ´æ°ãã¾ãã åå EFIãã¼ãã£ã·ã§ã³ãé¸æããã¦ãã¾ãããå¦çãã­ã£ã³ã»ã«ãã¾ããã é¸æããããã¼ãã£ã·ã§ã³ã¯ããã¾ããããã­ã»ã¹ã¯ã­ã£ã³ã»ã«ããã¾ããã ãªãã·ã§ã³ ãã¼ãã£ã·ã§ã³ãã¼ãã« ãã¼ããã°ã©ããã¤ããã©ã ããªã©ãå¾©åãã ã¿ã¤ã ã·ããã§ã·ã¹ãã ãå¾©åããï¼ã¿ã¤ã ã·ãããè¨­å®ããã¦ããå ´åã«éãã¾ãï¼ ãã¼ããgrubãããã³initrdãå¾©åãã ç°¡åã«è¨ãã°ããã£ã¹ã¯ã®æåã«åã³ã°ã©ããè¨é²ããã ãã§ãã ãµã¤ãº ã©ã¤ãã¢ã¼ãã®ãã¼ãã¯EFIã¢ã¼ããä½¿ç¨ãã¦ãã¾ããããã®ã³ã³ãã¥ã¼ã¿ã¼ã«ã¯<span foreground='red'>EFIãã¼ãã£ã·ã§ã³ãè¦ã¤ããã¾ããã§ãã</span>ããããããgrubã®å¾©åã¯ãã¾ããããªãã§ãããã

ã³ã³ãã¥ã¼ã¿ã¼ã®BIOSãä»ãã¦åèµ·åããã¬ã¬ã·ã¼ã¾ãã¯BIOSã¢ã¼ãã§ãã¼ãããããã«è¨­å®ãã¦ã¿ã¦ãã ããã

å°ãã§ããã¾ãããå¯è½æ§ãããã¨ãã¦ããè©¦ãã¦ã¿ã¾ããï¼ ãã®ã¦ã¼ãã£ãªãã£ã¯ã©ã¤ãã¢ã¼ãã§ã®ã¿ä½¿ç¨ããå¿è¦ãããã¾ãï¼ ã¿ã¤ã ã·ãããã¤ã³ã¹ãã¼ã«ããã¦ãã¾ããï¼ å¾ã£ã¦ãã ãã... ããªãã®ã³ã³ãã¥ã¼ã¿ã¼ã¯ã¤ã³ã¿ã¼ãããã«æ¥ç¶ããã¦ãã¾ããããªãã·ã§ã³2ã¨3ã¯ã¤ã³ã¿ã¼ããããå¿è¦ã§ãã 