��    #      4      L      L  �  M    �  �  �  �   �  �   /	  �   �	  9  l
  9  �  7  �  *     |   C     �     �     �  F   �  C     9   b  B   �  k   �     K  ,   P  (   }     �     �  +   �  C   �  "   -  @   P     �  .  �  .   �     �       V       l  p  t  �   �  4  �  g     �   l  �   �    t  2  �  y  �  @   6  �   w                 P     N   n  A   �  G   �  i   G     �  3   �  0   �           !   .   /   G   ^   #   �   F   �      !  l  !  6   �"     �"     �"  a   �"   <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 Deze tool moet worden gebruikt in de live-modus en om de manjaro-boot te herstellen die is geïnstalleerd op de harde schijf of SSD.

Als het geïnstalleerde systeem correct opstart, zijn er geen problemen met de opstart, dus het is beter om het gebruik van deze tool niet voort te zetten.

Waarschijnlijk werkt het ook met andere Linux-distributies.

Wil je doorgaan? Bevestig de gegevens:

Opstartmodus: efi
EFI-partitie: $efi_partition

Geselecteerde partitie: $selected_partition
Geselecteerd systeem: $selected_os
Partitieformaat: $partition_format
Partitie-UUID: $uuid_partition

Wilt u doorgaan? Bevestig de gegevens:

Opstartmodus: legacy/bios
Geselecteerde schijf: $disk_selected
Schijf partitietabel: $disk_table
Schijfgrootte: $disk_size
Geselecteerde partitie: $selected_partition
Geselecteerd systeem: $selected_os
Partitieformaat: $partition_format
Partitie-UUID: $uuid_partition

Wilt u doorgaan? Selecteer een optie:
Ik heb meer dan één EFI-partitie gevonden, selecteer welke gebruikt moet worden: Selecteer een optie:

Ik heb meer dan één Linux gevonden op deze computer, selecteer welke moet worden hersteld om op te starten: Selecteer een optie:
Ik heb meer dan één opslagapparaat gevonden, selecteer welke moet worden gebruikt om de opstart op te nemen: Selecteer de gewenste optie:

Herstel kan op 3 manieren worden uitgevoerd (eenvoudig, gemiddeld of volledig), als u twijfelt, probeer dan eerst de eerste optie, herstart de computer en kijk of het probleem is opgelost.

Als het probleem aanhoudt, probeer dan de andere opties. Selecteer de gewenste optie:

Herstel kan op 3 manieren worden uitgevoerd (<span foreground='gray'>eenvoudig, gemiddeld of volledig</span>), als u twijfelt, probeer dan de eerste optie, herstart de computer en kijk of u het probleem heeft opgelost.

Als het probleem aanhoudt, probeer dan de andere opties. De boot in live modus gebruikt de legacy modus, ook wel bios genoemd, <span foreground='red'>maar ik heb ten minste één efi-partitie gevonden op deze computer</span>, waarschijnlijk zal de herstelling van grub niet werken.

Probeer opnieuw op te starten en via de computer bios de boot in efi-modus te zetten.

Wil je het toch proberen, ook al is de kans klein dat het werkt? Verzamel bruikbare logbestanden van het geïnstalleerde systeem. Voltooi, voert de stappen van de tussenliggende restauratie uit, werkt het systeem bij en controleert of de lts-kernel is geïnstalleerd. Beschrijving Schijf Gedaan! Ik heb geen geïnstalleerde Linux gevonden op deze computer, proces geannuleerd. Interactief, opent een tui bedieningscentrum binnen het geselecteerde systeem. Interactief, opent een terminal binnen het geselecteerde systeem. Interactief, opent de pakketbeheerder binnen het geselecteerde systeem. Tussenliggend, herinstalleert het grub pakket in de partitie, genereert de configuratie en update initrd. Naam Geen EFI-partitie geselecteerd, proces geannuleerd. Geen geselecteerde partitie, geannuleerd proces. Optie Partitietabel Herstel opstart, grub, initrd en andere dingen Herstel systeem met timeshift (alleen als timeshift geconfigureerd is). Herstel de opstart, grub en initrd. Eenvoudig, neem gewoon de grub opnieuw op aan het begin van de schijf. Grootte De opstart in live modus gebruikt de efi modus, maar <span foreground='red'>ik heb geen efi partitie gevonden</span> op deze computer, waarschijnlijk zal de herstelling van grub niet werken.

Probeer opnieuw op te starten en via de bios van de computer de opstart in legacy of bios modus te zetten.

Wil je het toch proberen, ook al is de kans klein dat het werkt? Deze tool mag alleen gebruikt worden in de live modus! Timeshift niet geïnstalleerd! Wacht... Uw computer is niet verbonden met het internet, opties 2 en 3 hebben internet nodig om te werken. 