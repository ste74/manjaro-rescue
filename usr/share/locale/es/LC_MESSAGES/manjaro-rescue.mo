��    #      4      L      L  �  M    �  �  �  �   �  �   /	  �   �	  9  l
  9  �  7  �  *     |   C     �     �     �  F   �  C     9   b  B   �  k   �     K  ,   P  (   }     �     �  +   �  C   �  "   -  @   P     �  .  �  .   �     �       V       l  h  t  �   �  W  �  �   5  �   �  y       �  L    �  b  2   �  ~        �     �     �  R   �  K     ?   Y  H   �  d   �     G   <   N   /   �      �      �   6   �   H   !  %   X!  D   ~!     �!  V  �!  8   "#     [#  	   {#  c   �#   <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 Esta herramienta debe ser utilizada en modo en vivo y para restaurar el arranque de Manjaro instalado en el disco duro o SSD. Si el sistema instalado se está iniciando correctamente, no hay problemas en el arranque, por lo que es mejor no continuar usando esta herramienta. Probablemente también funcione con otras distribuciones de Linux. ¿Desea continuar? Confirmar los datos:

Modo de arranque: efi
Partición efi: $efi_partition

Partición seleccionada: $selected_partition
Sistema seleccionado: $selected_os
Formato de partición: $partition_format
UUID de la partición: $uuid_partition

¿Desea continuar? Confirmar los datos:

Modo de arranque: legacy/bios
Disco seleccionado: $disk_selected
Tabla de particiones del disco: $disk_table
Tamaño del disco: $disk_size
Partición seleccionada: $selected_partition
Sistema seleccionado: $selected_os
Formato de la partición: $partition_format
UUID de la partición: $uuid_partition

¿Desea continuar? <span font='16' foreground='lightskyblue'>seleccionar una opción:</span>

encontré más de una partición efi, selecciona cuál debería usar: <span font='16' foreground='lightskyblue'>seleccionar una opción:</span>

encontré más de una instalación de Linux en esta computadora, seleccione cuál debe restaurar el arranque: selecciona una opción: He encontrado más de un dispositivo de almacenamiento, selecciona cuál debe grabar el arranque: selecciona la opción deseada:

la restauración se puede hacer de 3 maneras (<span foreground='gray'>simple, intermedia o completa</span>), si tienes dudas, prueba la primera opción, reinicia la computadora y ve si se resolvió.

si el problema persiste, prueba las otras opciones. <span font='16' foreground='lightskyblue'>seleccionar la opción deseada:</span>

la restauración se puede hacer de 3 formas (<span foreground='gray'>simple, intermedia o completa</span>), si tienes dudas, prueba la primera opción, reinicia la computadora y ve si se resolvió.

si el problema persiste, prueba las otras opciones. Arrancar en modo en vivo está utilizando el modo heredado, también llamado BIOS, <span foreground='red'>pero encontré al menos una partición EFI en esta computadora</span>, probablemente la restauración de GRUB no funcionará.

Intenta reiniciar y a través de la BIOS de la computadora hacer el arranque en modo EFI.

¿Quieres intentarlo incluso con una pequeña probabilidad de éxito? Recopilar registros útiles del sistema instalado. Completa, realiza las etapas de la restauración intermedia, actualiza el sistema y verifica si el kernel lts está instalado. Descripción Disco ¡Hecho! No encontré ninguna instalación de Linux en esta computadora, proceso cancelado. Interactivo, abre un centro de control tui dentro del sistema seleccionado. Interactivo, abre una terminal dentro del sistema seleccionado. Interactivo, abre el gestor de paquetes dentro del sistema seleccionado. Intermedio, reinstala el paquete grub en la partición, genera la configuración y actualiza initrd. Nombre No se seleccionó la partición EFI, se canceló el proceso. Sin partición seleccionada, proceso cancelado. Opción Tabla de particiones Restaurar el arranque, grub, initrd y otros elementos. Restaurar el sistema con timeshift (solo si timeshift está configurado) Restaurar el arranque, grub e initrd. Sencillo, simplemente graba el grub de nuevo al principio del disco. Tamaño El arranque en modo en vivo está utilizando el modo EFI, pero no encontré ninguna partición EFI en esta computadora, probablemente la restauración de grub no funcionará.

Intenta reiniciar y a través de la BIOS de la computadora hacer el arranque en modo heredado o BIOS.

¿Quieres intentarlo incluso con pocas posibilidades de éxito? ¡Esta utilidad solo debe ser utilizada en modo en vivo! ¡Timeshift no está instalado! Espera... Tu computadora no está conectada a internet, las opciones 2 y 3 necesitan internet para funcionar. 