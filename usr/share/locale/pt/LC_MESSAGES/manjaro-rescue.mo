��    #      4      L      L  �  M    �  �  �  �   �  �   /	  �   �	  9  l
  9  �  7  �  *     |   C     �     �     �  F   �  C     9   b  B   �  k   �     K  ,   P  (   }     �     �  +   �  C   �  "   -  @   P     �  .  �  .   �     �       V       l  M  t  �   �  �  �  ]   �  z   �  l   y    �       k  %  )   �  {   �     7     C     I  K   P  I   �  ;   �  H   "   a   k      �   7   �   3   
!     >!     F!  +   [!  O   �!     �!  ;   �!     3"  o  ;"  7   �#     �#  	   �#  g   $   <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 Esta ferramenta deve ser usada em modo ao vivo e para restaurar o boot do Manjaro instalado no HD ou SSD. Se o sistema instalado estiver iniciando corretamente, não há problemas no boot, então é melhor não continuar usando esta ferramenta. Provavelmente também funciona com outras distribuições Linux. Você deseja continuar? Confirmar os dados:

Modo de inicialização: efi
Partição efi: $efi_partition

Partição selecionada: $selected_partition
Sistema selecionado: $selected_os
Formato da partição: $partition_format
UUID da partição: $uuid_partition

Deseja continuar? <span font='16' foreground='lightskyblue'>confirmar os dados:</span>    

<span font='11' foreground='gray'>modo de inicialização:</span>  legacy/bios
<span font='11' foreground='gray'>disco selecionado:</span> $disk_selected
    
<span font='11' foreground='gray'>tabela de partição do disco:</span> $disk_table
    
<span font='11' foreground='gray'>tamanho do disco:</span> $disk_size
   
<span font='11' foreground='gray'>partição selecionada:</span> $selected_partition
<span font='11' foreground='gray'>sistema selecionado:</span> $selected_os
<span font='11' foreground='gray'>formato da partição:</span> $partition_format
<span font='11' foreground='gray'>uuid da partição:</span> $uuid_partition

deseja continuar? selecione uma opção: 

encontrei mais de uma partição efi, selecione qual deve ser usada: selecione uma opção: 

encontrei mais de um Linux instalado neste computador, selecione qual deve ser restaurado o boot: selecione uma opção: encontrei mais de um dispositivo de armazenamento, selecione qual deve gravar o boot: selecione a opção desejada: 

a restauração pode ser feita de 3 maneiras (<span foreground='gray'>simples, intermediária ou completa</span>), se tiver dúvidas, tente a primeira opção, reinicie o computador e veja se resolveu.

se o problema persistir, tente as outras opções. selecione a opção desejada:

a restauração pode ser feita de 3 maneiras (<span foreground='gray'>simples, intermediária ou completa</span>), se estiver em dúvida, tente a primeira opção, reinicie o computador e veja se resolveu.

se o problema persistir, tente as outras opções. Iniciar em modo ao vivo está usando o modo legado, também chamado de bios, <span foreground='red'>mas encontrei pelo menos uma partição efi neste computador</span>, provavelmente a restauração do grub não funcionará.

Tente reiniciar e através da bios do computador faça o boot em modo efi.

Você quer tentar mesmo com pouca probabilidade de funcionar? Coletar logs úteis do sistema instalado. Completa, executa as etapas da restauração intermediária, atualiza o sistema e verifica se o kernel lts está instalado. Descrição Disco Feito! Não encontrei nenhum Linux instalado neste computador, processo cancelado. Interativo, abre um centro de controle tui dentro do sistema selecionado. Interativo, abre um terminal dentro do sistema selecionado. Interativo, abre o gerenciador de pacotes dentro do sistema selecionado. Intermediário, reinstala o pacote grub na partição, gera a configuração e atualiza o initrd. Nome Nenhuma partição EFI selecionada, processo cancelado. Nenhuma partição selecionada, processo cancelado. Opção Tabela de partição Restaurar boot, grub, initrd e outros itens Restaurar o sistema com o timeshift (apenas se o timeshift estiver configurado) Restaurar o boot, grub e initrd Simples, basta gravar o grub novamente no início do disco. Tamanho A inicialização em modo ao vivo está usando o modo efi, mas <span foreground='red'>não encontrei nenhuma partição efi</span> neste computador, provavelmente a restauração do grub não funcionará.

Tente reiniciar e, através da bios do computador, faça a inicialização em modo legacy ou bios.

Você quer tentar mesmo com pouca probabilidade de funcionar? Este utilitário deve ser usado apenas no modo ao vivo! Timeshift não instalado! Espera... Seu computador não está conectado à internet, as opções 2 e 3 precisam de internet para funcionar. 