��    #      4      L      L  �  M    �  �  �  �   �  �   /	  �   �	  9  l
  9  �  7  �  *     |   C     �     �     �  F   �  C     9   b  B   �  k   �     K  ,   P  (   }     �     �  +   �  C   �  "   -  @   P     �  .  �  .   �     �       V       l  �  t  l    �  r  �   K  �   �  �   �  �  �  �  �  |  v   Z   �"  �   N#     $     $     $  �   ,$  r   �$  a   7%  r   �%  �   &     �&  S   �&  Q   '     _'     n'  Q   �'     �'  >   `(  S   �(     �(  9   )     :+  &   �+     �+  �   �+   <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 Цей інструмент слід використовувати в режимі реального часу та для відновлення завантаження Manjaro, встановленого на жорсткий диск або SSD.

Якщо встановлена система запускається правильно, то немає проблем з завантаженням, тому краще не продовжувати використання цього інструменту.

Можливо, він також працює з іншими дистрибутивами Linux.

Ви хочете продовжити? Підтвердіть дані:
Режим завантаження: efi
Розділ efi: $efi_partition
Обраний розділ: $selected_partition
Обраний операційна система: $selected_os
Формат розділу: $partition_format
Ідентифікатор розділу: $uuid_partition

Бажаєте продовжити? Підтвердіть дані:

Режим завантаження: спадщина/біос
Вибраний диск: $disk_selected

Таблиця розділів диска: $disk_table

Розмір диска: $disk_size

Вибраний розділ: $selected_partition
Вибрана система: $selected_os
Формат розділу: $partition_format
Ідентифікатор розділу: $uuid_partition

Бажаєте продовжити? виберіть варіант:
Я знайшов більше однієї EFI-партіції, виберіть, яку слід використовувати: виберіть опцію:
Я знайшов більше однієї встановленої версії Linux на цьому комп'ютері, виберіть, яку потрібно відновити для завантаження: оберіть опцію:
Я знайшов більше одного пристрою зберігання, оберіть, який з них повинен записувати завантаження: Оберіть бажану опцію: відновлення можливе у 3 способи (<span foreground='gray'>простий, проміжний або повний</span>), якщо у вас є сумніви, спробуйте першу опцію, перезапустіть комп'ютер і переконайтеся, що проблему вирішено.

Якщо проблема продовжується, спробуйте інші опції. Оберіть бажану опцію: відновлення можливе за 3 способами (<span foreground='gray'>простий, проміжний або повний</span>), якщо у вас є сумніви, спробуйте першу опцію, перезапустіть комп'ютер і перевірте, чи вирішили ви проблему.

Якщо проблема продовжується, спробуйте інші опції. Завантаження в режимі живого режиму використовує спадковий режим, також відомий як BIOS, <span foreground='red'>але я знайшов принаймні один розділ EFI на цьому комп'ютері</span>, ймовірно, відновлення grub не працюватиме.

Спробуйте перезапустити комп'ютер і через BIOS змінити режим завантаження на режим EFI.

Чи хочете ви спробувати навіть з малою ймовірністю успіху? Збирайте корисні журнали з встановленої системи. Завершує, виконує етапи проміжного відновлення, оновлює систему та перевіряє, чи встановлено lts ядро. Опис Диск Зроблено! Я не знайшов жодної встановленої системи Linux на цьому комп'ютері, скасовано процес. Інтерактивний, відкриває центр керування tui в обраній системі. Інтерактивний, відкриває термінал в обраній системі. Інтерактивний, відкриває менеджер пакетів у вибраній системі. Середній рівень, перевстановлює пакет grub на розділі, генерує конфігурацію та оновлює initrd. Ім'я Не обрано жодної EFI-розділу, процес скасовано. Не обрано жодного розділу, процес скасовано. Варіант Таблиця розділів Відновити завантаження, grub, initrd та інші речі. Відновити систему за допомогою timeshift (тільки якщо timeshift налаштований). Відновити завантаження, grub та initrd. Просто зареєструйте grub знову в початку диска. Розмір Загрузка у режимі live використовує efi режим, але <span foreground='red'>я не знайшов жодної efi-партіції</span> на цьому комп'ютері, ймовірно, відновлення grub не працюватиме.

спробуйте перезавантажити та через біос комп'ютера переключити загрузку в режим спадщини або біосу.

хочете спробувати навіть з малою ймовірністю успіху? Ця утиліта повинна використовуватися тільки в режимі реального часу! Timeshift не встановлено! Зачекай... Ваш комп'ютер не підключений до Інтернету, для роботи опцій 2 та 3 потрібен Інтернет. 