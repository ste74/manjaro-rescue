��    #      4      L      L  �  M    �  �  �  �   �  �   /	  �   �	  9  l
  9  �  7  �  *     |   C     �     �     �  F   �  C     9   b  B   �  k   �     K  ,   P  (   }     �     �  +   �  C   �  "   -  @   P     �  .  �  .   �     �       V       l  o  t  �   �    �  [   �  x   8  �   �  P  J  R  �  \  �  2   K  ~   ~     �     	       K     E   b  9   �  =   �  m         �  ,   �  '   �  
   �     �  1      J   7   !   �   4   �      �   6  �   1   "     J"  	   j"  a   t"   <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 Denna verktyget bör användas i live-läge och för att återställa Manjaro-booten som är installerad på hårddisken eller SSD-enheten.

Om det installerade systemet startar korrekt finns inga problem med booten, så det är bättre att inte fortsätta använda detta verktyg.

Det fungerar förmodligen också med andra Linux-distributioner.

Vill du fortsätta? Bekräfta data:
Startläge:
efi
efi-partition:
$efi_partition
Vald partition:
$selected_partition
Valt system:
$selected_os
Partitionens format:
$partition_format
Partitionens UUID:
$uuid_partition

Vill du fortsätta? Bekräfta data:

Startläge:
legacy/bios
Vald disk: $disk_selected

Diskpartitionstabell: $disk_table

Diskstorlek: $disk_size

Vald partition: $selected_partition
Valt system: $selected_os
Partitionens format: $partition_format
Partitionens UUID: $uuid_partition

Vill du fortsätta? välj ett alternativ:
jag hittade mer än en EFI-partition, välj vilken som ska användas: Välj ett alternativ:
Jag har hittat mer än en Linux installerad på datorn, välj vilken som ska återställa starten: <span font='16' foreground='lightskyblue'>välj ett alternativ:</span>

jag hittade fler än en lagringsenhet, välj vilken som ska spela in uppstarten: <span font='16' foreground='lightskyblue'>välj önskad alternativ:</span>

återställning kan göras på 3 sätt (<span foreground='gray'>enkel, mellanliggande eller komplett</span>), om du är osäker, prova det första alternativet, starta om datorn och se om problemet löstes.

om problemet kvarstår, prova de andra alternativen. <span font='16' foreground='lightskyblue'>välj önskad alternativ:</span>

återställning kan göras på 3 sätt (<span foreground='gray'>enkel, mellanliggande eller komplett</span>), om du är osäker, prova det första alternativet, starta om datorn och se om du löste problemet.

om problemet kvarstår, prova de andra alternativen. Starta i live-läge använder legacy-läge, även kallat bios,
<span foreground='red'>men jag hittade åtminstone en efi-partition på datorn</span>,
troligtvis kommer inte grub återställning att fungera.

försök att starta om och genom datorns bios välj att starta i efi-läge.

vill du försöka även om det är osannolikt att det fungerar? Samla användbara loggar från installerat system. Slutför, utför stegen för mellanliggande restaurering, uppdaterar systemet och kontrollerar om lts-kärnan är installerad. Beskrivning Skiva Klart! Jag hittade ingen installerad linux på den här datorn, avbröt processen. Interaktivt, öppnar ett tui-kontrollcenter inuti det valda systemet. Interaktivt, öppnar en terminal inom det valda systemet. Interaktivt, öppnar pakethanteraren inom det valda systemet. Mellanliggande, återinstallerar grub-paketet i partitionen, genererar konfigurationen och uppdaterar initrd. Namn Ingen efi-partition vald, avbröt processen. Ingen vald partition, avbruten process. Alternativ Partitions tabell Återställ boot, grub, initrd och annat material Återställ systemet med timeshift (endast om timeshift är konfigurerat). Återställ boot, grub och initrd Enkelt, spela bara in grub igen i början av disken. Storlek Stöveln i live-läge använder efi-läge, men jag hittade ingen efi-partition på den här datorn,
troligtvis kommer inte grub-restaurering att fungera.

Försök att starta om och genom datorns bios välj att starta i legacy eller bios-läge.

Vill du försöka även om det är liten chans att det fungerar? Denna verktyg bör endast användas i live-läge! Timeshift är inte installerat! Vänta... Din dator är inte ansluten till internet, alternativ 2 och 3 behöver internet för att fungera. 