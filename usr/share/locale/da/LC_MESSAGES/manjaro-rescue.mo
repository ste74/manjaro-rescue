��    #      4      L      L  �  M    �  �  �  �   �  �   /	  �   �	  9  l
  9  �  7  �  *     |   C     �     �     �  F   �  C     9   b  B   �  k   �     K  ,   P  (   }     �     �  +   �  C   �  "   -  @   P     �  .  �  .   �     �       V       l  S  t  �   �    �  Z   �     &  c   �    
      P    6   e  �   �          *     3  Q   <  E   �  <   �  B     d   T     �  -   �  &   �            )   ,  C   V     �  9   �  
   �  8  �  0   4!     e!     �!  o   �!   <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 Dette værktøj bør bruges i live-tilstand og til at genoprette den installerede Manjaro-boot på hd eller ssd.

Hvis det installerede system starter korrekt, er der ingen problemer med boot, så det er bedst ikke at fortsætte brugen af dette værktøj.

Det virker sandsynligvis også med andre Linux-distributioner.

Vil du fortsætte? Bekræft dataene:

Boot-tilstand:
efi
EFI-partition:
$efi_partition

Valgt partition:
$selected_partition
Valgt system:
$selected_os
Partition format:
$partition_format
Partition UUID:
$uuid_partition

Ønsker du at fortsætte? Bekræft dataene:

Boot-tilstand:
Legacy/BIOS
Valgt disk:
$disk_selected
Disk-partitionstabel:
$disk_table
Disk-størrelse:
$disk_size
Valgt partition:
$selected_partition
Valgt system:
$selected_os
Partition-format:
$partition_format
Partition UUID:
$uuid_partition

Vil du fortsætte? Vælg en mulighed:

Jeg har fundet mere end én EFI-partition, vælg den, der skal bruges: Vælg en mulighed:
Jeg har fundet mere end én Linux installeret på denne computer, vælg hvilken der skal gendanne opstarten: Vælg en mulighed:
Jeg har fundet mere end én lagerenhed, vælg hvilken der skal optage opstarten: Vælg den ønskede mulighed: 

Gendannelse kan gøres på 3 måder (simpel, mellem eller komplet), hvis du er i tvivl, så prøv den første mulighed, genstart computeren og se om problemet er løst. 

Hvis problemet fortsætter, så prøv de andre muligheder. Vælg den ønskede mulighed:

Gendannelse kan udføres på 3 måder (simpel, mellem eller komplet), hvis du er i tvivl, så prøv den første mulighed, genstart computeren og se om problemet er løst.

Hvis problemet fortsætter, så prøv de andre muligheder. Boot i live-tilstand bruger legacy-tilstand, også kaldet bios,
<span foreground='red'>men jeg fandt mindst en efi-partition på denne computer</span>,
sandsynligvis vil grub-restaurering ikke virke.

Prøv at genstarte og gennem computerens bios vælg boot i efi-tilstand.

Vil du prøve, selvom det er lidt sandsynligt, at det virker? Indsamle nyttige logfiler fra det installerede system. Fuldfører, udfører trinnene i den mellemliggende restaurering, opdaterer systemet og kontrollerer om lts-kernen er installeret. Beskrivelse Diskette Færdig! Jeg fandt ikke nogen Linux installeret på denne computer, annullerede processen. Interaktivt, åbner et tui kontrolcenter inden for det valgte system. Interaktivt, åbner en terminal inden for det valgte system. Interaktiv, åbner pakkehåndteringen inden for det valgte system. Mellemtrin, geninstallerer grub-pakken i partitionen, genererer konfigurationen og opdaterer initrd. Navn Ingen efi partition valgt, annulleret proces. Ingen valgt partition, afbrudt proces. Mulighed Partitionstabel Genopret boot, grub, initrd og andre ting Gendan systemet med timeshift (kun hvis timeshift er konfigureret). Gendan boot, grub og initrd Simpelt, bare optag grub'en igen i begyndelsen af disken. Størrelse Støvlen i live-tilstand bruger efi-tilstand, men jeg fandt ikke nogen efi-partition på denne computer. Det er sandsynligt, at grub-restaurering ikke vil virke.

Prøv at genstarte og gennem computerens bios vælg boot i legacy eller bios-tilstand.

Vil du prøve, selvom det er lidt sandsynligt, at det virker? Dette værktøj bør kun bruges i live-tilstand! Timeshift ikke installeret! Vent... Din computer er ikke forbundet til internettet, mulighederne 2 og 3 kræver internetforbindelse for at fungere. 