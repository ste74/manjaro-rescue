��    #      4      L      L  �  M    �  �  �  �   �  �   /	  �   �	  9  l
  9  �  7  �  *     |   C     �     �     �  F   �  C     9   b  B   �  k   �     K  ,   P  (   }     �     �  +   �  C   �  "   -  @   P     �  .  �  .   �     �       V       l  i  t  �   �  '  �  U   �  �   @  `   �  -  #  .  Q  ?  �  +   �  �   �     n     z     �  J   �  G   �  =     C   Y  h   �       +     '   7  
   _     j  2   {  G   �  %   �  7      
   T   X  _   .   �!     �!     "  c   
"   <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 Dette verktøyet bør brukes i live-modus og for å gjenopprette manjaro oppstarten installert på harddisk eller SSD.

Hvis det installerte systemet starter riktig, er det ingen problemer med oppstarten, så det er bedre å ikke fortsette å bruke dette verktøyet.

Det vil sannsynligvis også fungere med andre Linux-distribusjoner.

Ønsker du å fortsette? Bekreft dataene:

Oppstartsmåte: efi
Efi-partisjon: $efi_partition

Valgt partisjon: $selected_partition
Valgt system: $selected_os
Partisjonsformat: $partition_format
Partisjons-UUID: $uuid_partition

Ønsker du å fortsette? Bekreft dataene:

Oppstartsmåte:
legacy/bios
Valgt disk:
$disk_selected

Diskpartisjonstabell:
$disk_table

Diskstørrelse:
$disk_size

Valgt partisjon:
$selected_partition
Valgt system:
$selected_os
Partisjonsformat:
$partition_format
Partisjons-UUID:
$uuid_partition

Ønsker du å fortsette? Velg et alternativ:
Jeg fant mer enn én EFI-partisjon, velg hvilken som skal brukes: Velg et alternativ:

Jeg fant mer enn én Linux installert på denne datamaskinen, velg hvilken som skal gjenopprette oppstarten: velg et alternativ: Jeg fant mer enn én lagringsenhet, velg hvilken som skal ta opp oppstarten: velg ønsket alternativ:

gjenoppretting kan gjøres på 3 måter (<span foreground='gray'>enkel, mellomliggende eller komplett</span>), hvis du er i tvil, prøv det første alternativet, start datamaskinen på nytt og se om du løste problemet.

hvis problemet vedvarer, prøv de andre alternativene. velg ønsket alternativ:
 
gjenoppretting kan gjøres på 3 måter (<span foreground='gray'>enkel, mellomliggende eller komplett</span>), hvis du er i tvil, prøv det første alternativet, start datamaskinen på nytt og se om du løste problemet.

hvis problemet vedvarer, prøv de andre alternativene. Oppstart i live-modus bruker legacy-modus, også kalt bios, men jeg fant minst en efi-partisjon på denne datamaskinen, sannsynligvis vil ikke grub-restaurering fungere.

Prøv å starte på nytt og gjennom datamaskinens bios velg å starte i efi-modus.

Vil du prøve selv om det er lite sannsynlig at det vil fungere? Samle nyttige logger fra installert system. Fullfører, utfører stadiene for den mellomliggende restaureringen, oppdaterer systemet og sjekker om lts-kjernen er installert. Beskrivelse Plate Ferdig! Jeg fant ingen Linux installert på denne datamaskinen, avbrøt prosessen. Interaktivt, åpner et tui kontrollsenter innenfor det valgte systemet. Interaktivt, åpner en terminal innenfor det valgte systemet. Interaktivt, åpner pakkehåndtereren innenfor det valgte systemet. Mellomliggende, installerer grub-pakken på partisjonen, genererer konfigurasjonen og oppdaterer initrd. Navn Ingen efi-partisjon valgt, avbrutt prosess. Ingen valgt partisjon, avbrutt prosess. Alternativ Partisjonstabell Gjenopprett oppstart, grub, initrd og annet utstyr Gjenopprett systemet med timeshift (kun hvis timeshift er konfigurert). Gjenopprett oppstart, grub og initrd. Enkelt, bare ta opp grub igjen i begynnelsen av disken. Størrelse Støvelen i live-modus bruker efi-modus, men <span foreground='red'>jeg fant ingen efi-partisjon</span> på denne datamaskinen,
sannsynligvis vil ikke grub restaurering fungere.

prøv å starte på nytt og gjennom datamaskinens bios endre støvelen til legacy eller bios-modus.

vil du prøve selv om det er lite sannsynlig at det vil fungere? Dette verktøyet bør kun brukes i live-modus! Timeshift ikke installert! Vent... Din datamaskin er ikke koblet til internett, alternativene 2 og 3 trenger internett for å fungere. 