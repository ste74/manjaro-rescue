��    #      4      L      L  �  M    �  �  �  �   �  �   /	  �   �	  9  l
  9  �  7  �  *     |   C     �     �     �  F   �  C     9   b  B   �  k   �     K  ,   P  (   }     �     �  +   �  C   �  "   -  @   P     �  .  �  .   �     �       V       l  q  t    �  �  �  �   �  �   :  �   �  :  �  Z  �  �    2   �   �   �   
   d!  ;  o!     �#  A   �#  D   �#  9   ?$  @   y$  k   �$     &%  3   ,%  )   `%     �%     �%  9   �%  M   �%  +   +&  =   W&     �&  �  �&  =   +(     i(     �(  Y   �(   <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 Bu araç canlı modda kullanılmalı ve hd veya ssd'ye kurulu olan Manjaro önyükleyicisini geri yüklemek için kullanılmalıdır.

Eğer kurulu sistem doğru bir şekilde başlıyorsa, önyüklemede sorun yoktur, bu yüzden bu aracın kullanımına devam etmek daha iyi olur.

Muhtemelen diğer Linux dağıtımlarıyla da çalışır.

Devam etmek istiyor musunuz? <span font='16' foreground='lightskyblue'>verileri onayla:</span>

<span font='11' foreground='gray'>önyükleme modu:</span> efi
<span font='11' foreground='gray'>efi bölümü:</span> $efi_partition

<span font='11' foreground='gray'>seçilen bölüm:</span> $selected_partition
<span font='11' foreground='gray'>seçilen sistem:</span> $selected_os
<span font='11' foreground='gray'>bölüm biçimi:</span> $partition_format
<span font='11' foreground='gray'>bölüm uuid:</span> $uuid_partition

devam etmek istiyor musunuz? <span font='16' foreground='lightskyblue'>verileri onayla:</span>    

<span font='11' foreground='gray'>önyükleme modu:</span>  eski/BIOS
<span font='11' foreground='gray'>seçili disk:</span> $disk_selected
    
<span font='11' foreground='gray'>disk bölüm tablosu:</span> $disk_table
    
<span font='11' foreground='gray'>disk boyutu:</span> $disk_size
   
<span font='11' foreground='gray'>seçili bölüm:</span> $selected_partition
<span font='11' foreground='gray'>seçili sistem:</span> $selected_os
<span font='11' foreground='gray'>bölüm biçimi:</span> $partition_format
<span font='11' foreground='gray'>bölüm uuid:</span> $uuid_partition

devam etmek istiyor musunuz? <span font='16' foreground='lightskyblue'>bir seçenek seçin:</span>

Birden fazla efi bölümü buldum, hangisinin kullanılacağını seçin: <span font='16' foreground='lightskyblue'>bir seçenek seçin:</span>

Bu bilgisayarda birden fazla Linux buldum, önyüklemeyi hangisinin geri yüklenmesi gerektiğini seçin: <span font='16' foreground='lightskyblue'>bir seçenek seçin:</span>

birden fazla depolama cihazı buldum, hangisinin önyükleme kaydedeceğini seçin: <span font='16' foreground='lightskyblue'>istenen seçeneği seçin:</span>

Onarım 3 şekilde yapılabilir (<span foreground='gray'>basit, orta veya tam</span>), şüphe varsa, ilk seçeneği deneyin, bilgisayarı yeniden başlatın ve çözüldüğünü görün.

Sorun devam ederse diğer seçenekleri deneyin. <span font='16' foreground='lightskyblue'>istenen seçeneği seçin:</span>

Onarım 3 farklı şekilde yapılabilir (<span foreground='gray'>basit, orta veya tam</span>), şüphe varsa, ilk seçeneği deneyin, bilgisayarı yeniden başlatın ve sorununuzun çözülüp çözülmediğini görün.

Sorun devam ederse, diğer seçenekleri deneyin. Canlı modda önyükleme, eski moda olarak da bilinen bios kullanılarak yapılır, <span foreground='red'>ancak bu bilgisayarda en az bir efi bölümü buldum</span>, muhtemelen grub onarımı çalışmayacaktır.

Bilgisayarı yeniden başlatmayı deneyin ve bilgisayar biosu aracılığıyla efi modunda önyükleme yapın.

Çalışma ihtimali düşük olsa bile denemek ister misiniz? Yüklü sistemden yararlı günlükleri toplayın. Tamamla, ara restorasyonun aşamalarını gerçekleştirir, sistemi günceller ve lts çekirdeğinin yüklü olup olmadığını kontrol eder. Açıklama Disk

Disk, bilgisayar donanımı olarak bilinen bir depolama aygıtıdır. Disk, verilerin okunması, yazılması ve saklanması için kullanılır. Bilgisayarların yanı sıra, dijital kameralar, oyun konsolları ve diğer elektronik cihazlar da disk kullanır. Diskler, sabit diskler ve optik diskler olmak üzere iki türde gelir. Sabit diskler, verilerin manyetik olarak depolanması için kullanılırken, optik diskler, verilerin optik olarak depolanması için kullanılır. Her iki tür disk de bilgisayar kullanıcıları için önemli bir depolama aracıdır. Tamamlandı! Bu bilgisayarda kurulu bir Linux bulamadım, işlemi iptal ettim. Etkileşimli, seçilen sistem içinde bir tui kontrol merkezi açar. Etkileşimli, seçilen sistem içinde bir terminal açar. Etkileşimli, seçilen sistem içinde paket yöneticisini açar. Orta düzey, bölümde grub paketini yeniden yükler, yapılandırmayı oluşturur ve initrd'yi günceller. İsim Seçili bir efi bölümü yok, işlem iptal edildi. Seçili bölüm yok, işlem iptal edildi. Seçenek Bölüm tablosu Önyükleme, grub, initrd ve diğer şeyleri geri yükle. Timeshift ile sistem geri yükle (yalnızca timeshift yapılandırılmışsa) Önyükleme, grub ve initrd'yi geri yükle. Basit, sadece diskin başlangıcında grub'u tekrar kaydedin. Boyut Canlı modda başlatılan önyükleme, efi modunu kullanıyor, ancak bu bilgisayarda <span foreground='red'>herhangi bir efi bölümü bulamadım</span>, muhtemelen grub onarımı çalışmayacaktır.

Bilgisayarı yeniden başlatmayı deneyin ve bilgisayarın bios ayarlarından önyükleme modunu legacy veya bios olarak değiştirin.

Çalışma ihtimali düşük olsa bile denemek ister misiniz? Bu yardımcı program sadece canlı modda kullanılmalıdır! Timeshift kurulu değil! Bekle... Bilgisayarınız internete bağlı değil, 2 ve 3 seçenek internete bağlı çalışır. 