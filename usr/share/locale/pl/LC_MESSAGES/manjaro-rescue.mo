��    #      4      L      L  �  M    �  �  �  �   �  �   /	  �   �	  9  l
  9  �  7  �  *     |   C     �     �     �  F   �  C     9   b  B   �  k   �     K  ,   P  (   }     �     �  +   �  C   �  "   -  @   P     �  .  �  .   �     �       V       l  �  t  �   �  �  �  `   �  �   �  r   �  @     9  A  �  {  6   �  k   3     �     �     �  Q   �  ?      3   C   ?   w   n   �      &!  +   ,!  )   X!     �!     �!  /   �!  T   �!     "  ;   ="     y"  e  �"  =   �#  !   %$     G$  q   S$   <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 Ten narzędzie powinno być używane w trybie live i do przywracania rozruchu systemu Manjaro zainstalowanego na dysku twardym lub dysku SSD.

Jeśli zainstalowany system uruchamia się poprawnie, nie ma problemów z rozruchem, więc lepiej nie kontynuować korzystania z tego narzędzia.

Prawdopodobnie będzie również działać z innymi dystrybucjami Linux.

Czy chcesz kontynuować? Potwierdź dane:
    
Tryb uruchamiania: efi
Partycja efi: $efi_partition
    
Wybrana partycja: $selected_partition
Wybrany system: $selected_os
Format partycji: $partition_format
UUID partycji: $uuid_partition

Czy chcesz kontynuować? <span font='16' foreground='lightskyblue'>potwierdź dane:</span>

<span font='11' foreground='gray'>tryb rozruchu:</span>  legacy/bios
<span font='11' foreground='gray'>wybrany dysk:</span> $disk_selected
    
<span font='11' foreground='gray'>tabela partycji dysku:</span> $disk_table
    
<span font='11' foreground='gray'>rozmiar dysku:</span> $disk_size
   
<span font='11' foreground='gray'>wybrana partycja:</span> $selected_partition
<span font='11' foreground='gray'>wybrany system:</span> $selected_os
<span font='11' foreground='gray'>format partycji:</span> $partition_format
<span font='11' foreground='gray'>uuid partycji:</span> $uuid_partition

czy chcesz kontynuować? wybierz opcję:
znalazłem więcej niż jedną partycję efi, wybierz którą powinienem użyć: wybierz opcję: znalazłem więcej niż jedną instalację systemu Linux na tym komputerze, wybierz, którą chcesz przywrócić jako system startowy: wybierz opcję: znalazłem więcej niż jedno urządzenie przechowywania, wybierz które powinno zapisać rozruch: Wybierz pożądaną opcję: 

odtworzenie można wykonać na 3 sposoby (<span foreground='gray'>prosty, pośredni lub kompletny</span>), jeśli masz wątpliwości, spróbuj pierwszej opcji, zrestartuj komputer i sprawdź, czy rozwiązałeś problem.

Jeśli problem będzie się utrzymywał, spróbuj pozostałych opcji. Wybierz pożądaną opcję: 

Przywracanie można wykonać na 3 sposoby (<span foreground='gray'>prosty, pośredni lub kompletny</span>), jeśli masz wątpliwości, spróbuj pierwszej opcji, zrestartuj komputer i sprawdź, czy rozwiązałeś problem.

Jeśli problem nadal występuje, spróbuj pozostałych opcji. Uruchomienie w trybie live jest wykonywane w trybie legacy, nazywanym również bios,
<span foreground='red'>ale znalazłem przynajmniej jedną partycję efi na tym komputerze</span>,
prawdopodobnie przywracanie grub nie będzie działać.

Spróbuj ponownie uruchomić komputer i przez bios przełączyć się w tryb efi.

Czy chcesz spróbować mimo niewielkich szans na powodzenie? Zbieraj przydatne dzienniki z zainstalowanego systemu. Wykonuje etapy pośredniego odzyskiwania, aktualizuje system i sprawdza, czy zainstalowane jest jądro lts. Opis Dysk Gotowe! Nie znalazłem zainstalowanego systemu Linux na tym komputerze, anulowano proces. Interaktywny, otwiera centrum kontroli tui w wybranym systemie. Interaktywne, otwiera terminal w wybranym systemie. Interaktywny, otwiera menedżera pakietów w wybranym systemie. Średnio zaawansowany, ponownie instaluje pakiet grub w partycji, generuje konfigurację i aktualizuje initrd. Imię Nie wybrano partycji EFI, anulowano proces. Brak wybranej partycji, anulowany proces. Opcja Tabela partycji Przywróć rozruch, grub, initrd i inne rzeczy. Przywróć system za pomocą timeshift (tylko jeśli timeshift jest skonfigurowany). Przywróć boot, grub i initrd. Proste, po prostu zapisz ponownie Gruba na początku dysku. Rozmiar But w trybie live, but używany jest tryb efi, ale <span foreground='red'>nie znalazłem żadnej partycji efi</span> na tym komputerze, prawdopodobnie przywracanie grub nie będzie działać.

Spróbuj zrestartować i w ustawieniach biosu komputera zmienić tryb uruchamiania na legacy lub bios.

Czy chcesz spróbować mimo niewielkich szans na powodzenie? Ten narzędzie powinno być używane tylko w trybie na żywo! Timeshift nie jest zainstalowany! Poczekaj... Twój komputer nie jest połączony z internetem, opcje 2 i 3 wymagają połączenia z internetem, aby działać. 