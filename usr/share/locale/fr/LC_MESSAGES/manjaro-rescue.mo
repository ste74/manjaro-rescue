��    #      4      L      L  �  M    �  �  �  �   �  �   /	  �   �	  9  l
  9  �  7  �  *     |   C     �     �     �  F   �  C     9   b  B   �  k   �     K  ,   P  (   }     �     �  +   �  C   �  "   -  @   P     �  .  �  .   �     �       V       l  �  t  9  �  �  /  �     �   �  �   �  �    �  �  q     4   �!  �   �!     E"     Q"  
   X"  l   c"  L   �"  =   #  L   [#  n   �#     $  7   $  3   S$     �$     �$  ;   �$  N   �$  '   ,%  F   T%     �%  M  �%  9   �&     *'     J'  l   V'   <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 Cet outil devrait être utilisé en mode direct et pour restaurer le démarrage de Manjaro installé sur un disque dur ou un SSD.

Si le système installé démarre correctement, il n'y a pas de problèmes de démarrage, il vaut donc mieux ne pas continuer à utiliser cet outil.

Il est probable qu'il fonctionne également avec d'autres distributions Linux.

Voulez-vous continuer ? <span font='16' foreground='lightskyblue'>Confirmer les données:</span>
    
<span font='11' foreground='gray'>Mode de démarrage:</span> efi
<span font='11' foreground='gray'>Partition efi:</span> $efi_partition
    
<span font='11' foreground='gray'>Partition sélectionnée:</span> $selected_partition
<span font='11' foreground='gray'>Système sélectionné:</span> $selected_os
<span font='11' foreground='gray'>Format de la partition:</span> $partition_format
<span font='11' foreground='gray'>UUID de la partition:</span> $uuid_partition

Voulez-vous continuer? <span font='16' foreground='lightskyblue'>Confirmer les données:</span>    

<span font='11' foreground='gray'>Mode de démarrage:</span>  legacy/bios
<span font='11' foreground='gray'>Disque sélectionné:</span> $disk_selected
    
<span font='11' foreground='gray'>Table de partition du disque:</span> $disk_table
    
<span font='11' foreground='gray'>Taille du disque:</span> $disk_size
   
<span font='11' foreground='gray'>Partition sélectionnée:</span> $selected_partition
<span font='11' foreground='gray'>Système sélectionné:</span> $selected_os
<span font='11' foreground='gray'>Format de la partition:</span> $partition_format
<span font='11' foreground='gray'>UUID de la partition:</span> $uuid_partition

Voulez-vous continuer? <span font='16' foreground='lightskyblue'>sélectionner une option :</span>

j'ai trouvé plus d'une partition efi, sélectionnez celle qui devrait être utilisée : <span font='16' foreground='lightskyblue'>sélectionner une option :</span>

J'ai trouvé plus d'un système Linux installé sur cet ordinateur, sélectionnez celui qui devrait restaurer le démarrage : Sélectionnez une option : j'ai trouvé plus d'un périphérique de stockage, sélectionnez celui qui doit enregistrer le démarrage : <span font='16' foreground='lightskyblue'>sélectionnez l'option souhaitée:</span>

La restauration peut être effectuée de 3 façons (<span foreground='gray'>simple, intermédiaire ou complète</span>), si vous avez des doutes, essayez d'abord la première option, redémarrez l'ordinateur et voyez si le problème est résolu.

Si le problème persiste, essayez les autres options. <span font='16' foreground='lightskyblue'>sélectionnez l'option souhaitée:</span>

La restauration peut être effectuée de 3 manières (<span foreground='gray'>simple, intermédiaire ou complète</span>), si vous avez des doutes, essayez la première option, redémarrez l'ordinateur et voyez si vous avez résolu le problème.

Si le problème persiste, essayez les autres options. Le démarrage en mode live utilise le mode hérité, également appelé bios, mais j'ai trouvé au moins une partition efi sur cet ordinateur, il est probable que la restauration de grub ne fonctionnera pas.

Essayez de redémarrer et de passer par le bios de l'ordinateur pour démarrer en mode efi.

Voulez-vous essayer même si cela a peu de chances de fonctionner ? Collectez des journaux utiles du système installé. Compléter, effectue les étapes de la restauration intermédiaire, met à jour le système et vérifie si le noyau lts est installé. Description Disque Terminé ! Je n'ai trouvé aucun système d'exploitation Linux installé sur cet ordinateur, j'ai annulé le processus. Interactif, ouvre un centre de contrôle tui dans le système sélectionné. Interactif, ouvre un terminal dans le système sélectionné. Interactif, ouvre le gestionnaire de paquets dans le système sélectionné. Intermédiaire, réinstalle le paquet grub dans la partition, génère la configuration et met à jour initrd. Nom Aucune partition efi sélectionnée, processus annulé. Aucune partition sélectionnée, processus annulé. Option Table de partition Restaurer le démarrage, grub, initrd et autres éléments. Restaurer le système avec timeshift (uniquement si timeshift est configuré). Restaurer le démarrage, grub et initrd Simple, enregistrez simplement le grub à nouveau au début du disque. Taille Le démarrage en mode live utilise le mode efi, mais je n'ai pas trouvé de partition efi sur cet ordinateur, il est probable que la restauration de grub ne fonctionnera pas.

Essayez de redémarrer et de passer en mode legacy ou bios dans le bios de l'ordinateur.

Voulez-vous essayer même si cela a peu de chances de fonctionner ? Cet utilitaire ne doit être utilisé qu'en mode direct ! Timeshift n'est pas installé ! Attendez... Votre ordinateur n'est pas connecté à internet, les options 2 et 3 ont besoin d'internet pour fonctionner. 