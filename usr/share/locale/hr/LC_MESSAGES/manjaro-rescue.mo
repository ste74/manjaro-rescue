��    #      4      L      L  �  M    �  �  �  �   �  �   /	  �   �	  9  l
  9  �  7  �  *     |   C     �     �     �  F   �  C     9   b  B   �  k   �     K  ,   P  (   }     �     �  +   �  C   �  "   -  @   P     �  .  �  .   �     �       V       l  8  t  �   �  5  �  �   �  �   `  �     .  �  .  �  t    3   �  a   �          #     (  P   0  C   �  7   �  A   �  \   ?     �  /   �  )   �     �        )      E   =      �   9   �   	   �   ]  �   >   @"     "     �"  U   �"   <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 Ovaj alat treba koristiti u živom načinu rada i za vraćanje Manjaro boot-a instaliranog na tvrdom disku ili SSD-u.

Ako instalirani sustav pokreće ispravno, nema problema s boot-om, pa je bolje ne nastavljati upotrebu ovog alata.

Vjerojatno će raditi i s drugim Linux distribucijama.

Želite li nastaviti? Potvrdite podatke:

Način pokretanja: efi
Particija efi: $efi_partition

Odabrana particija: $selected_partition
Odabrani sustav: $selected_os
Format particije: $partition_format
UUID particije: $uuid_partition

Želite li nastaviti? Potvrdite podatke:

Način pokretanja:
legacy/bios
Odabrani disk: $disk_selected

Tablica particija diska: $disk_table

Veličina diska: $disk_size

Odabrana particija: $selected_partition
Odabrani sustav: $selected_os
Format particije: $partition_format
UUID particije: $uuid_partition

Želite li nastaviti? <span font='16' foreground='lightskyblue'>odaberite opciju:</span>

Pronašao sam više od jedne EFI particije, odaberite koju želite koristiti: <span font='16' foreground='lightskyblue'>odaberite opciju:</span>

pronašao sam više od jednog instaliranog Linuxa na ovom računalu, odaberite koji bi trebao obnoviti pokretanje: <span font='16' foreground='lightskyblue'>odaberite opciju:</span>

pronašao sam više od jednog uređaja za pohranu, odaberite koji će snimati pokretanje: Odaberite željenu opciju:

Obnova se može izvršiti na 3 načina (<span foreground='gray'>jednostavno, srednje ili potpuno</span>), ako ste u nedoumici, pokušajte prvu opciju, ponovno pokrenite računalo i provjerite jeste li riješili problem.

Ako se problem nastavi, pokušajte s drugim opcijama. Odaberite željenu opciju:

Obnova se može izvršiti na 3 načina (<span foreground='gray'>jednostavno, srednje ili potpuno</span>), ako ste u nedoumici, pokušajte prvu opciju, ponovno pokrenite računalo i provjerite jeste li riješili problem.

Ako se problem nastavi, pokušajte s drugim opcijama. Pokretanje u živom načinu rada koristi legacy način, također nazvan bios,
<span foreground='red'>ali pronašao sam barem jednu efi particiju na ovom računalu</span>,
vjerovatno obnavljanje grub-a neće raditi.

pokušajte ponovno pokrenuti računalo i putem bios-a postaviti pokretanje u efi načinu.

želite li pokušati čak i ako je malo vjerojatno da će raditi? Sakupljajte korisne zapise iz instaliranog sustava. Završi, izvršava faze srednje obnove, ažurira sustav i provjerava je li instaliran lts kernel. Opis Disk Gotovo! Nisam pronašao nijednu instaliranu linux na ovom računalu, otkazao sam proces. Interaktivno, otvara tui kontrolni centar unutar odabranog sustava. Interaktivno, otvara terminal unutar odabranog sustava. Interaktivno, otvara upravitelja paketa unutar odabranog sustava. Srednji, ponovno instalira paket grub u particiji, generira konfiguraciju i ažurira initrd. Ime Nije odabrana EFI particija, proces je otkazan. Nema odabrane particije, prekinut proces. Opcija Tablica particija Vrati boot, grub, initrd i ostale stvari. Vrati sustav pomoću timeshifta (samo ako je timeshift konfiguriran). Vrati boot, grub i initrd. Jednostavno, samo ponovno snimite grub na početku diska. Veličina "Čizma u živom načinu rada koristi efi način, ali <span foreground='red'>nisam pronašao nijednu efi particiju</span> na ovom računalu, vjerojatno obnova grub-a neće raditi.

pokušajte ponovno pokrenuti računalo i putem biosa postaviti pokretanje u legacy ili bios načinu.

želite li pokušati čak i ako je malo vjerojatno da će raditi?" Ovaj alat bi trebao biti korišten samo u živom načinu rada! Timeshift nije instaliran! Pričekaj... Vaše računalo nije povezano s internetom, opcije 2 i 3 zahtijevaju internet za rad. 