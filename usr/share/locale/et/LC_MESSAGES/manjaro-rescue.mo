��    #      4      L      L  �  M    �  �  �  �   �  �   /	  �   �	  9  l
  9  �  7  �  *     |   C     �     �     �  F   �  C     9   b  B   �  k   �     K  ,   P  (   }     �     �  +   �  C   �  "   -  @   P     �  .  �  .   �     �       V       l  w  t    �  =  �  ^   /  �   �  n     �   ~  <  a  e  �  1     q   6  	   �     �     �  O   �  ;     1   S  3   �  R   �       1     /   C     s     y  -   �  H   �  !      0   $      U   K  \   4   �!     �!     �!  ]   "   <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 See tööriist peaks olema kasutatud otse režiimis ja Manjaro käivitamise taastamiseks, mis on paigaldatud kõvakettale või SSD-le.

Kui paigaldatud süsteem käivitub korrektselt, siis pole käivitamisega probleeme ja seetõttu on parem mitte selle tööriista kasutamist jätkata.

Tõenäoliselt töötab see ka teiste Linuxi distributsioonidega.

Kas soovite jätkata? Kinnita andmed:

Käivitusrežiim:

efi
efi partitsioon:

$efi_partitsioon

Valitud partitsioon:

$valitud_partitsioon
Valitud süsteem:

$valitud_os
Partitsiooni vormindamine:

$partitsiooni_vorming
Partitsiooni UUID:

$uuid_partitsioon

Kas soovite jätkata? Kinnita andmed:

Käivitusrežiim:
pärand/BIOS
Valitud ketas: $disk_selected

Ketta partitsioonitabel:
$disk_table

Ketta suurus:
$disk_size

Valitud partitsioon:
$selected_partition
Valitud süsteem:
$selected_os
Partitsiooni vormindamine:
$partition_format
Partitsiooni UUID:
$uuid_partition

Kas soovite jätkata? Vali üks võimalus:
Leidsin rohkem kui ühe EFI partitsiooni, vali, millist peaksin kasutama: Vali üks võimalus:

Leidsin sellest arvutist rohkem kui ühe Linuxi installi, vali, milline neist peaks taastama käivitamise. Vali üks võimalus:
Leidsin rohkem kui ühe salvestusseadme, vali, milline neist peaks käivitust salvestama: Vali soovitud valik:

Taastamine saab teha 3 viisil (lihtne, keskmine või täielik), kui oled kahtlev, proovi esimest valikut, taaskäivita arvuti ja vaata, kas probleem lahendati.

Kui probleem püsib, proovi teisi valikuid. <span font='16' foreground='lightskyblue'>Vali soovitud valik:</span>
        
Taastamine saab teha 3 viisil (<span foreground='gray'>lihtne, vahepealne või täielik</span>), kui oled kahtlev, proovi esimest valikut, taaskäivita arvuti ja vaata, kas probleem lahendati.

Kui probleem püsib, proovi teisi valikuid. Käivitamine live-režiimis kasutab pärandrežiimi, mida nimetatakse ka BIOS-iks,
<span foreground='red'>kuid leidsin vähemalt ühe EFI partitsiooni sellel arvutil</span>,
tõenäoliselt ei tööta grub taastamine.

proovi taaskäivitada ja läbi arvuti BIOS-i käivitada efi režiimis.

kas soovid proovida isegi väikese tõenäosusega, et see töötab? Koguge kasulikke logisid paigaldatud süsteemist. Täielik, täidab vahepealse taastamise etapid, värskendab süsteemi ja kontrollib, kas on paigaldatud lts tuum. Kirjeldus ette

Ketast Tehtud! Ma ei leidnud ühtegi Linuxi installitud sellel arvutil, tühistasin protsessi. Interaktiivne, avab valitud süsteemis tui juhtimiskeskuse. Interaktiivne, avab valitud süsteemis terminali. Interaktiivne, avab pakihalduri valitud süsteemis. Kesktase, taastab grub paketi osas, genereerib konfiguratsiooni ja uuendab initrd. Nimi Ei valitud EFI partitsiooni, protsess tühistati. Valitud partitsiooni pole, protsess tühistati. Valik Partitsioonitabel Taasta käivitus, grub, initrd ja muud asjad. Taasta süsteem ajamasinaga (ainult juhul, kui ajamasin on seadistatud). Taasta käivitus, grub ja initrd. Lihtsalt salvestage grub uuesti kettale alguses. Suurus Saabas elurežiimis kasutatav saabas kasutab EFI režiimi, kuid selle arvuti peal ei leidnud ma ühtegi EFI partitsiooni, tõenäoliselt ei tööta ka grub taastamine.

Proovi arvuti taaskäivitada ja läbi arvuti BIOS-i seada saabas pärand- või BIOS režiimi.

Kas soovid proovida isegi väikese tõenäosusega, et see töötab? See programm peaks kasutama ainult režiimis "live"! Timeshift pole paigaldatud! Oota... Sinu arvuti pole ühendatud internetiga, võimalused 2 ja 3 vajavad töötamiseks internetti. 