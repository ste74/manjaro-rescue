��    #      4      L      L  �  M    �  �  �  �   �  �   /	  �   �	  9  l
  9  �  7  �  *     |   C     �     �     �  F   �  C     9   b  B   �  k   �     K  ,   P  (   }     �     �  +   �  C   �  "   -  @   P     �  .  �  .   �     �       V       l  L  t     �  &  �  j   	  �   t  u     �   �    |  C  �  6   �  t     	   x     �     �  \   �  C   �  :   5  A   p  g   �        3      /   S      �      �   /   �   U   �   !   !  *   A!     l!  W  t!  7   �"     #  	   #  \   )#   <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 Alat ini harus digunakan dalam mode langsung dan untuk memulihkan boot Manjaro yang terinstal di hd atau ssd.

Jika sistem yang terinstal berjalan dengan benar, tidak ada masalah dalam boot, jadi lebih baik tidak melanjutkan penggunaan alat ini.

Mungkin juga bekerja dengan distribusi Linux lainnya.

Apakah Anda ingin melanjutkan? <span font='16' foreground='lightskyblue'>kinnita andmed:</span>

<span font='11' foreground='gray'>käivitusrežiim:</span> efi
<span font='11' foreground='gray'>efi partitsioon:</span> $efi_partitsioon

<span font='11' foreground='gray'>valitud partitsioon:</span> $valitud_partitsioon
<span font='11' foreground='gray'>valitud süsteem:</span> $valitud_os
<span font='11' foreground='gray'>partitsiooni vorming:</span> $partitsiooni_vorming
<span font='11' foreground='gray'>partitsiooni uuid:</span> $uuid_partitsioon

kas soovite jätkata? Sahkan data ini:

Mode boot:
warisan/BIOS
Disk terpilih: $disk_terpilih

Jadual bahagian disk:
$jadual_disk

Saiz disk:
$saiz_disk

Bahagian terpilih:
$bahagian_terpilih
Sistem terpilih:
$os_terpilih
Format bahagian:
$format_bahagian
UUID bahagian:
$uuid_bahagian

Adakah anda ingin meneruskan? Pilih opsi berikut ini:

Saya menemukan lebih dari satu partisi EFI, pilih yang mana yang harus digunakan: Pilih opsi berikut ini:

Saya menemukan lebih dari satu sistem operasi Linux yang terpasang di komputer ini, pilih yang mana yang harus dipulihkan boot-nya: Pilih opsi berikut ini:
Saya menemukan lebih dari satu perangkat penyimpanan, pilih yang mana yang akan merekam boot: Pilih pilihan yang diinginkan: pemulihan dapat dilakukan dalam 3 cara (sederhana, menengah, atau lengkap), jika ragu, coba pilihan pertama, restart komputer dan lihat apakah masalah teratasi.

Jika masalah masih berlanjut, coba pilihan lainnya. Pilih opsi yang diinginkan: pemulihan dapat dilakukan dengan 3 cara (<span foreground='gray'>sederhana, menengah, atau lengkap</span>), jika ragu, coba opsi pertama, restart komputer dan lihat apakah masalah teratasi.

Jika masalah masih berlanjut, coba opsi lainnya. Boot dalam mode langsung menggunakan mode warisan, juga disebut bios, tetapi saya menemukan setidaknya satu partisi efi di komputer ini, kemungkinan pemulihan grub tidak akan berhasil.

Coba restart dan melalui bios komputer membuat boot dalam mode efi.

Apakah Anda ingin mencoba meskipun kemungkinan kecil untuk berhasil? Kumpulkan log yang berguna dari sistem yang terpasang. Lengkap, melaksanakan tahap-tahap restorasi menengah, memperbarui sistem, dan memeriksa apakah kernel lts terpasang. Kirjeldus Diskijas Selesai! Saya tidak menemukan sistem operasi Linux yang terpasang di komputer ini, proses dibatalkan. Interaktif, membuka pusat kontrol tui di dalam sistem yang dipilih. Interaktif, membuka terminal di dalam sistem yang dipilih. Interaktif, membuka pengelola paket di dalam sistem yang dipilih. Perantara, memasang semula pakej grub dalam bahagian, menghasilkan konfigurasi dan mengemaskini initrd. Nafn Tiada bahagian efi yang dipilih, proses dibatalkan. Tiada bahagian yang dipilih, proses dibatalkan. Pilihan Tabel partisi Taastage käivitus, grub, initrd ja muud asjad. Taastage süsteem koos timeshiftiga (ainult juhul, kui timeshift on konfigureeritud). Kembalikan boot, grub, dan initrd Mudah, hanya rekam grub lagi di awal disk. Stærð Støvelen i live-modus bruker efi-modus, men <span foreground='red'>jeg fant ingen efi-partisjon</span> på denne datamaskinen,
sannsynligvis vil ikke grub restaurering fungere.

prøv å starte på nytt og gjennom datamaskinens bios gjøre støvelen i legacy eller bios-modus.

vil du prøve selv om det er lite sannsynlig at det vil fungere? Perkakas ini hanya boleh digunakan dalam mode langsung! Timeshift tidak terpasang! Tunggu... Komputer anda tidak terhubung ke internet, opsi 2 dan 3 memerlukan internet untuk berfungsi. 