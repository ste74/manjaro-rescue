��    #      4      L      L  �  M    �  �  �  �   �  �   /	  �   �	  9  l
  9  �  7  �  *     |   C     �     �     �  F   �  C     9   b  B   �  k   �     K  ,   P  (   }     �     �  +   �  C   �  "   -  @   P     �  .  �  .   �     �       V       l  T  t  $  �  �  �  �   �  �   f  �   @  �  �  �  z  �     8   �!  ~   �!     u"     ~"     �"  P   �"  S   �"  F   0#  J   w#  }   �#     @$  =   E$  7   �$     �$     �$  :   �$  c   %  3   }%  :   �%     �%  w  �%  4   k'     �'     �'  �   �'   <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 Ez az eszköz élő módban és a Manjaro boot helyreállítására szolgál, amely telepítve van a merevlemezre vagy SSD-re.

Ha a telepített rendszer megfelelően indul, nincsenek problémák a bootolással, ezért jobb, ha nem folytatja az eszköz használatát.

Valószínűleg más Linux disztribúciókkal is működik.

Folytatja? <span font='16' foreground='lightskyblue'>adatok megerősítése:</span>

<span font='11' foreground='gray'>indítási mód:</span> efi
<span font='11' foreground='gray'>efi partíció:</span> $efi_partition
    
<span font='11' foreground='gray'>kiválasztott partíció:</span> $selected_partition
<span font='11' foreground='gray'>kiválasztott rendszer:</span> $selected_os
<span font='11' foreground='gray'>partíció formátuma:</span> $partition_format
<span font='11' foreground='gray'>partíció uuid-je:</span> $uuid_partition

folytatja? <span font='16' foreground='lightskyblue'>Adatok megerősítése:</span>    

<span font='11' foreground='gray'>Boot mód:</span>  legacy/bios
<span font='11' foreground='gray'>Kiválasztott lemez:</span> $disk_selected
    
<span font='11' foreground='gray'>Lemez partíciós tábla:</span> $disk_table
    
<span font='11' foreground='gray'>Lemez mérete:</span> $disk_size
   
<span font='11' foreground='gray'>Kiválasztott partíció:</span> $selected_partition
<span font='11' foreground='gray'>Kiválasztott rendszer:</span> $selected_os
<span font='11' foreground='gray'>Partíció formátuma:</span> $partition_format
<span font='11' foreground='gray'>Partíció UUID-je:</span> $uuid_partition

Folytatni szeretné? <span font='16' foreground='lightskyblue'>válasszon egy lehetőséget:</span>

több mint egy efi partíciót találtam, válassza ki, melyiket kell használni: <span font='16' foreground='lightskyblue'>válasszon egy lehetőséget:</span>

több mint egy Linux található telepítve ezen a számítógépen, válassza ki, melyiket kell helyreállítani a rendszerindításhoz: <span font='16' foreground='lightskyblue'>válasszon egy lehetőséget:</span>

több tárolóeszközt találtam, válassza ki, melyik rögzítse a boot-ot: <span font='16' foreground='lightskyblue'>válassza ki a kívánt lehetőséget:</span>

a helyreállítás 3 módon végezhető el (<span foreground='gray'>egyszerű, köztes vagy teljes</span>), ha bizonytalan, próbálja ki az első lehetőséget, indítsa újra a számítógépet és nézze meg, hogy megoldódott-e a probléma.

ha a probléma továbbra is fennáll, próbálja ki a másik lehetőségeket. <span font='16' foreground='lightskyblue'>válassza ki a kívánt lehetőséget:</span>

a helyreállítás 3 módon történhet (<span foreground='gray'>egyszerű, köztes vagy teljes</span>), ha bizonytalan, próbálja ki az első lehetőséget, indítsa újra a számítógépet és nézze meg, hogy megoldódott-e a probléma.

ha a probléma továbbra is fennáll, próbálja ki a másik lehetőségeket. A live módban történő indítás hagyományos módban történik, amit BIOS-nak is neveznek,
<span foreground='red'>de legalább egy EFI partíciót találtam ezen a számítógépen</span>,
valószínűleg a grub helyreállítás nem fog működni.

Próbáld újraindítani és a számítógép BIOS-án keresztül EFI módban indítani.

Szeretnéd megpróbálni még akkor is, ha kicsi az esélye, hogy működni fog? Gyűjtsön hasznos naplókat a telepített rendszerből. Teljesíti az átmeneti helyreállítás fázisait, frissíti a rendszert és ellenőrzi, hogy telepítve van-e az LTS kernel. Leírás Lemez Kész! Nem találtam Linuxot telepítve ezen a számítógépen, folyamat megszakítva. Interaktív, megnyit egy tui vezérlőközpontot a kiválasztott rendszeren belül. Interaktív, megnyit egy terminált a kiválasztott rendszeren belül. Interaktív, megnyitja a csomagkezelőt a kiválasztott rendszeren belül. Középhaladó szinten újratelepíti a grub csomagot a partíción, létrehozza a konfigurációt és frissíti az initrd-t. Név Nincs kiválasztva EFI partíció, megszakítva a folyamatot. Nincs kiválasztott partíció, megszakított folyamat. Lehetőség Partíciós táblázat Állítsa vissza a boot, grub, initrd és egyéb dolgokat. Rendszer helyreállítása timeshift segítségével (csak akkor, ha a timeshift be van állítva). Állítsd vissza a boot, grub és initrd fájlokat. Egyszerű, csak rögzítsd újra a grubot a lemez elején. Méret Az élő módban a boot efi módot használ, de <span foreground='red'>nem találtam efi partíciót</span> ezen a számítógépen,
valószínűleg a grub helyreállítás nem fog működni.

Próbálja újraindítani és a számítógép bios-ában állítsa be a bootot legacy vagy bios módban.

Szeretné megpróbálni még akkor is, ha kicsi az esélye, hogy működik? Ez a segédprogram csak élő módban használható! Timeshift nincs telepítve! Várj... A számítógéped nincs csatlakoztatva az internethez, a 2. és 3. lehetőség internetkapcsolatot igényelnek a működéshez. 