Þ    #      4      L      L    M    ç  ®  ú     ©  ¥   /	     Õ	  9  l
  9  ¦  7  à  *     |   C     À     Ì     Ñ  F   ×  C     9   b  B     k   ß     K  ,   P  (   }     ¦     ­  +   ½  C   é  "   -  @   P       .    .   Å     ô       V       l    t  *  ø  [  #  x        ø  «     ¢  <    ß  Í  a  A   /   ~   q      ð   	   ÷      !  X   	!  X   b!  E   »!  O   "  ~   Q"     Ð"  N   ×"  G   &#     n#     u#  /   #  _   ¹#  3   $  T   M$     ¢$    ©$  B   +&  0   n&     &  z   ¬&   <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 ì´ ëêµ¬ë ë¼ì´ë¸ ëª¨ëìì ì¬ì©íê³  HD ëë SSDì ì¤ì¹ë ë§ìë¡ ë¶íì ë³µìíë ë° ì¬ì©í´ì¼ í©ëë¤.

ì¤ì¹ë ìì¤íì´ ì¬ë°ë¥´ê² ììëë©´ ë¶íì ë¬¸ì ê° ìì¼ë¯ë¡ ì´ ëêµ¬ë¥¼ ê³ì ì¬ì©íë ê²ì´ ì¢ì§ ììµëë¤.

ìë§ë ë¤ë¥¸ ë¦¬ëì¤ ë°°í¬íììë ìëí  ê²ìëë¤.

ê³ì ì§ííìê² ìµëê¹? <span font='16' foreground='lightskyblue'>ë°ì´í°ë¥¼ íì¸íìê² ìµëê¹:</span>

<span font='11' foreground='gray'>ë¶í ëª¨ë:</span> efi
<span font='11' foreground='gray'>efi íí°ì:</span> $efi_partition

<span font='11' foreground='gray'>ì íë íí°ì:</span> $selected_partition
<span font='11' foreground='gray'>ì íë ìì¤í:</span> $selected_os
<span font='11' foreground='gray'>íí°ì í¬ë§·:</span> $partition_format
<span font='11' foreground='gray'>íí°ì uuid:</span> $uuid_partition

ì§ííìê² ìµëê¹? ë°ì´í°ë¥¼ íì¸íìê² ìµëê¹?

ë¶í ëª¨ë: ë ê±°ì/BIOS
ì íë ëì¤í¬: $disk_selected
ëì¤í¬ íí°ì íì´ë¸: $disk_table
ëì¤í¬ í¬ê¸°: $disk_size
ì íë íí°ì: $selected_partition
ì íë ìì¤í: $selected_os
íí°ì í¬ë§·: $partition_format
íí°ì UUID: $uuid_partition

ì§ííìê² ìµëê¹? ìµìì ì ííì¸ì: 

ì¬ë¬ ê°ì efi íí°ìì ì°¾ììµëë¤. ì´ë¤ ê²ì ì¬ì©í ì§ ì ííì¸ì: ìµìì ì ííì¸ì:

ì´ ì»´í¨í°ìë ì¬ë¬ ê°ì ë¦¬ëì¤ê° ì¤ì¹ëì´ ììµëë¤. ë¶íì ë³µìí  ë¦¬ëì¤ë¥¼ ì ííì¸ì. <span font='16' foreground='lightskyblue'>ìµìì ì ííì¸ì:</span>

ì¬ë¬ ê°ì ì ì¥ ì¥ì¹ë¥¼ ì°¾ììµëë¤. ë¶íì ê¸°ë¡í  ì¥ì¹ë¥¼ ì ííì¸ì. <span font='16' foreground='lightskyblue'>ìíë ìµìì ì ííì¸ì:</span>

ë³µìì 3ê°ì§ ë°©ë²ì¼ë¡ í  ì ììµëë¤ (<span foreground='gray'>ê°ë¨í, ì¤ê° ë¨ê³ ëë ìì í</span>). ìì¬ì´ ë¤ ê²½ì°, ì²« ë²ì§¸ ìµìì ìëíê³  ì»´í¨í°ë¥¼ ì¬ììíì¬ ë¬¸ì ë¥¼ í´ê²°í  ì ìëì§ íì¸íì¸ì.

ë§ì½ ë¬¸ì ê° ì§ìëë¤ë©´ ë¤ë¥¸ ìµìì ìëíì¸ì. <span font='16' foreground='lightskyblue'>ìíë ìµìì ì ííì¸ì:</span>

ë³µìì 3ê°ì§ ë°©ë²(<span foreground='gray'>ê°ë¨, ì¤ê°, ìì </span>)ì¼ë¡ í  ì ììµëë¤. ìì¬ì´ ë¤ë©´ ì²« ë²ì§¸ ìµìì ì ííê³  ì»´í¨í°ë¥¼ ì¬ììíì¬ ë¬¸ì ê° í´ê²°ëëì§ íì¸íì¸ì.

ë§ì½ ë¬¸ì ê° ì§ìëë¤ë©´ ë¤ë¥¸ ìµìì ìëíì¸ì. ë¼ì´ë¸ ëª¨ëìì ë¶ííë ê²ì ë ê±°ì ëª¨ë, ì¦ ë°ì´ì¤ì¤ë¥¼ ì¬ì©í©ëë¤. <span foreground='red'>íì§ë§ ì´ ì»´í¨í°ìë ì ì´ë íëì EFI íí°ìì ë°ê²¬íìµëë¤</span>. ìë§ë ê·¸ë¼ìë ë¶êµ¬íê³  ê·¸ë½ ë³µêµ¬ë ìëíì§ ìì ê²ìëë¤.

ì»´í¨í°ë¥¼ ì¬ììíê³  ë°ì´ì¤ì¤ë¥¼ íµí´ ë¶íì EFI ëª¨ëë¡ ë³ê²½í´ë³´ì¸ì.

ìë ê°ë¥ì±ì´ ì ëë¼ë ìëí´ë³´ìê² ìµëê¹? ì¤ì¹ë ìì¤íìì ì ì©í ë¡ê·¸ë¥¼ ìì§íì­ìì¤. ìë£, ì¤ê° ë³µì ë¨ê³ë¥¼ ìííê³  ìì¤íì ìë°ì´í¸íë©° lts ì»¤ëì´ ì¤ì¹ëìëì§ íì¸í©ëë¤. ì¤ëª ëì¤í¬ ìë£! ì´ ì»´í¨í°ìë ì¤ì¹ë ë¦¬ëì¤ê° ìì´ì íë¡ì¸ì¤ë¥¼ ì·¨ìíìµëë¤. ì íí ìì¤í ë´ìì ìí¸ìì© ê°ë¥íë©°, tui ì ì´ ì¼í°ë¥¼ ì½ëë¤. ëíìì¼ë¡ ì íí ìì¤í ë´ìì í°ë¯¸ëì ì½ëë¤. ëíìì¼ë¡ ì íí ìì¤í ë´ìì í¨í¤ì§ ë§¤ëì ë¥¼ ì½ëë¤. ì¤ê° ë¨ê³ìì íí°ìì grub í¨í¤ì§ë¥¼ ë¤ì ì¤ì¹íê³  êµ¬ì±ì ìì±íë©° initrdë¥¼ ìë°ì´í¸í©ëë¤. ì´ë¦ EFI íí°ìì ì ííì§ ììì¼ë¯ë¡ íë¡ì¸ì¤ë¥¼ ì·¨ìí©ëë¤. ì íë íí°ìì´ ìì´ì íë¡ì¸ì¤ê° ì·¨ìëììµëë¤. ìµì íí°ì íì´ë¸ ë¶í¸, ê·¸ë½, initrd ë° ê¸°í í­ëª© ë³µì íìì¬íí¸ë¡ ìì¤í ë³µìíê¸° (íìì¬íí¸ê° ì¤ì ëì´ ìë ê²½ì°ìë§) ë¶í¸, ê·¸ë½ ê·¸ë¦¬ê³  initrdë¥¼ ë³µìíì¸ì. ê°ë¨íê² ëì¤í¬ì ìì ë¶ë¶ìì grubì ë¤ì ê¸°ë¡íë©´ ë©ëë¤. í¬ê¸° ë¼ì´ë¸ ëª¨ëìì ë¶íì efi ëª¨ëë¥¼ ì¬ì©íì§ë§, ì´ ì»´í¨í°ìë <span foreground='red'>efi íí°ìì ì°¾ì ì ìììµëë¤</span>. ìë§ë grub ë³µìì´ ìëíì§ ìì ê²ìëë¤.

ì¬ììíê³  ì»´í¨í° BIOSë¥¼ íµí´ ë¶íì ë ê±°ì ëë BIOS ëª¨ëë¡ ë³ê²½íì­ìì¤.

ìë ê°ë¥ì±ì´ ì ëë¼ë ìëí´ ë³´ìê² ìµëê¹? ì´ ì í¸ë¦¬í°ë ì¤ì  ëª¨ëììë§ ì¬ì©í´ì¼ í©ëë¤! íììíí¸ê° ì¤ì¹ëì§ ìììµëë¤! ê¸°ë¤ë ¤... ë¹ì ì ì»´í¨í°ë ì¸í°ë·ì ì°ê²°ëì´ ìì§ ììµëë¤. 2ë²ê³¼ 3ë² ìµìì ì¸í°ë·ì´ íìí©ëë¤. 