��    #      4      L      L  �  M    �  �  �  �   �  �   /	  �   �	  9  l
  9  �  7  �  *     |   C     �     �     �  F   �  C     9   b  B   �  k   �     K  ,   P  (   }     �     �  +   �  C   �  "   -  @   P     �  .  �  .   �     �       V       l    t  J  �  O  �  �   '  .  �  �     �       !  b  5#  `   �%  �   �%     �&     �&     �&  �   �&  x   �'  e   (  t   m(  �   �(     �)  B   �)  J   �)     %*     4*  P   T*  w   �*  <   +  W   Z+     �+    �+     �-  +   _.     �.  �   �.   <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 Этот инструмент должен использоваться в режиме реального времени и для восстановления загрузчика Manjaro, установленного на жесткий диск или твердотельный накопитель.

<span font='12' foreground='red'>Если установленная система запускается правильно, то проблем с загрузкой нет, поэтому лучше не продолжать использование этого инструмента.</span>
<span font='10' foreground='gray'>Возможно, также будет работать с другими дистрибутивами Linux.</span>

Хотите продолжить? Подтвердите данные:

Режим загрузки: efi
Раздел efi: $efi_partition

Выбранный раздел: $selected_partition
Выбранная система: $selected_os
Формат раздела: $partition_format
UUID раздела: $uuid_partition

Вы хотите продолжить? <span font='16' foreground='lightskyblue'>подтвердите данные:</span>    

<span font='11' foreground='gray'>режим загрузки:</span>  наследуемый/BIOS
<span font='11' foreground='gray'>выбранный диск:</span> $disk_selected
    
<span font='11' foreground='gray'>таблица разделов диска:</span> $disk_table
    
<span font='11' foreground='gray'>размер диска:</span> $disk_size
   
<span font='11' foreground='gray'>выбранный раздел:</span> $selected_partition
<span font='11' foreground='gray'>выбранная система:</span> $selected_os
<span font='11' foreground='gray'>формат раздела:</span> $partition_format
<span font='11' foreground='gray'>uuid раздела:</span> $uuid_partition

хотите продолжить? <span font='16' foreground='lightskyblue'>выберите опцию:</span>

я нашел более одной раздел efi, выберите, какой из них использовать: <span font='16' foreground='lightskyblue'>выберите опцию:</span>

я обнаружил несколько установленных версий Linux на этом компьютере, выберите, какую из них следует восстановить загрузчик: <span font='16' foreground='lightskyblue'>выберите вариант:</span>

я нашел более одного устройства хранения, выберите, какое из них должно записывать загрузку: Выберите желаемый вариант: восстановление может быть выполнено 3 способами (простым, промежуточным или полным), если есть сомнения, попробуйте первый вариант, перезагрузите компьютер и проверьте, решилась ли проблема.

Если проблема продолжается, попробуйте другие варианты. Выберите желаемый вариант: восстановление может быть выполнено тремя способами (<span foreground='gray'>простым, промежуточным или полным</span>), если есть сомнения, попробуйте первый вариант, перезагрузите компьютер и проверьте, решилась ли проблема.

Если проблема продолжается, попробуйте другие варианты. Загрузка в режиме live осуществляется в режиме наследования, также называемом BIOS, <span foreground='red'>но я нашел как минимум один раздел EFI на этом компьютере</span>, возможно, восстановление grub не сработает.

Попробуйте перезагрузить и через BIOS компьютера переключиться в режим загрузки в EFI.

Хотите попробовать даже с малой вероятностью успеха? Соберите полезные журналы из установленной системы. Завершает, выполняет этапы промежуточного восстановления, обновляет систему и проверяет, установлен ли ядро lts. Описание Диск Готово! Я не нашел установленной операционной системы Linux на этом компьютере, процесс отменен. Интерактивный, открывает центр управления tui в выбранной системе. Интерактивный, открывает терминал в выбранной системе. Интерактивный, открывает менеджер пакетов в выбранной системе. Промежуточный, переустанавливает пакет grub в разделе, генерирует конфигурацию и обновляет initrd. Имя Не выбран раздел efi, процесс отменен. Нет выбранного раздела, процесс отменен. Вариант Таблица разделов Восстановить загрузку, grub, initrd и другие вещи Восстановить систему с помощью timeshift (только если timeshift настроен). Восстановить загрузчик, grub и initrd. Просто запишите загрузчик снова в начало диска. Размер Загрузка в режиме live осуществляется через efi, но на этом компьютере я не обнаружил ни одного раздела efi. Вероятно, восстановление grub не будет работать.

Попробуйте перезагрузить компьютер и через биос настроить загрузку в режиме legacy или bios.

Хотите попробовать даже с небольшой вероятностью успеха? Эта утилита должна использоваться только в режиме реального времени! Таймшифт не установлен! Подожди... Ваш компьютер не подключен к интернету, для работы вариантов 2 и 3 необходимо наличие интернета. 