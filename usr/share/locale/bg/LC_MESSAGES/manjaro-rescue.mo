��    #      4      L      L  �  M    �  �  �  �   �  �   /	  �   �	  9  l
  9  �  7  �  *     |   C     �     �     �  F   �  C     9   b  B   �  k   �     K  ,   P  (   }     �     �  +   �  C   �  "   -  @   P     �  .  �  .   �     �       V       l  \  t  l  �  �  >  �     �   �  �   n  �  6  �  !  R     a   _"  �   �"     �#     �#     �#  o   �#  u   N$  ]   �$  n   "%  �   �%     -&  N   4&  G   �&     �&  &   �&  C   '  {   K'  @   �'  T   (     ](  %  j(  u   �*  .   +     5+  �   G+   <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 Този инструмент трябва да се използва в режим на живо и за възстановяване на Manjaro стартирането на хард диск или SSD.

Ако инсталираната система стартира правилно, няма проблеми със стартирането, затова е по-добре да не продължавате да използвате този инструмент.

Вероятно работи и с други дистрибуции на Linux.

Искате ли да продължите? Потвърдете данните:

Режим на зареждане: efi
efi раздел: $efi_partition

Избран раздел: $selected_partition
Избрана система: $selected_os
Формат на раздела: $partition_format
Идентификатор на раздела: $uuid_partition

Искате ли да продължите? Потвърдете данните:

Режим на зареждане:
legacy/bios
Избран диск:
$disk_selected

Таблица за разделите на диска:
$disk_table

Размер на диска:
$disk_size

Избран раздел:
$selected_partition
Избрана система:
$selected_os
Формат на раздела:
$partition_format
UUID на раздела:
$uuid_partition

Искате ли да продължите? изберете опция: 
намерих повече от един efi раздел, изберете кой да използвате: изберете опция: 

намерих повече от един инсталиран Linux на този компютър, изберете кой да възстанови стартирането: изберете опция: 
намерих повече от един устройство за съхранение, изберете кое от тях да запише стартирането: изберете желаната опция:

възстановяването може да се извърши по 3 начина (<span foreground='gray'>прост, среден или пълен</span>), ако имате съмнения, опитайте първата опция, рестартирайте компютъра и вижте дали сте решили проблема.

ако проблемът продължава, опитайте другите опции. изберете желаната опция:

възстановяването може да се извърши по 3 начина (<span foreground='gray'>прост, среден или пълен</span>), ако имате съмнения, опитайте първата опция, рестартирайте компютъра и вижте дали сте решили проблема.

ако проблемът продължава, опитайте другите опции. Стартирането в режим на живо използва легаси режим, наричан също биос,
<span foreground='red'>но открих поне един efi раздел на този компютър</span>,
вероятно възстановяването на grub няма да работи.

Опитайте да рестартирате и чрез биос на компютъра да направите стартирането в efi режим.

Искате ли да опитате дори с малка вероятност да работи? Събиране на полезни записи от инсталираната система. Завършете, изпълнявайте етапите на промеждинното възстановяване, актуализирайте системата и проверете дали е инсталирано lts ядрото. Описание Диск Готово! Не намерих инсталирана Linux на този компютър, отмених процеса. Интерактивно, отваря контролен център на tui в избраната система. Интерактивно, отваря терминал в избраната система. Интерактивно, отваря пакетния мениджър в избраната система. Средно, преинсталира пакета grub в раздела, генерира конфигурацията и актуализира initrd. Име Не е избрана efi раздела, процесът е отменен. Няма избран раздел, процесът е отменен. Възможност Таблица за разделяне Възстановете boot, grub, initrd и други неща. Възстановете системата с помощта на timeshift (само ако е конфигуриран). Възстановете буута, груба и инитрд. Просто запишете отново grub в началото на диска. Размер Бутът в режим на живо използва efi режим, но <span foreground='red'>не откривам никакви efi раздел</span> на този компютър,
вероятно възстановяването на grub няма да работи.

Опитайте да рестартирате и чрез биос на компютъра превключете бута в легаси или биос режим.

Искате ли да опитате дори с малка вероятност да успее? Тази помощна програма трябва да се използва само в реален режим! Таймшифт не е инсталиран! Изчакай... Вашият компютър не е свързан с интернет, опциите 2 и 3 изискват интернет за да работят. 