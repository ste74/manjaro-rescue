��    #      4      L      L  �  M    �  �  �  �   �  �   /	  �   �	  9  l
  9  �  7  �  *     |   C     �     �     �  F   �  C     9   b  B   �  k   �     K  ,   P  (   }     �     �  +   �  C   �  "   -  @   P     �  .  �  .   �     �       V       l  O  t  �   �  P  �  g     �   t  �       �    �  `  �  /   0  �   `  	   �     �     �  L   �  D   H  @   �  @   �  j        z  =     *   �     �     �  4      I   ;   ,   �   <   �      �   <  �   C   4"     x"     �"  j   �"   <span font='12'>This tool should be used in live mode and to restore the Manjaro boot
installed on HD or SSD.</span>

<span font='12' foreground='red'>If the installed system is starting correctly there are no problems in the boot, so it is
better not to continue the use of this tool.</span>
<span font='10' foreground='gray'>Probably also work with other Linux distributions.</span>

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>
    
<span font='11' foreground='gray'>Boot mode:</span> EFI
<span font='11' foreground='gray'>EFI partition:</span> $EFI_PARTITION
    
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Confirm the data:</span>    

<span font='11' foreground='gray'>Boot mode:</span>  LEGACY/BIOS
<span font='11' foreground='gray'>Selected disk:</span> $DISK_SELECTED
    
<span font='11' foreground='gray'>Disk partition table:</span> $DISK_TABLE
    
<span font='11' foreground='gray'>Disk Size:</span> $DISK_SIZE
   
<span font='11' foreground='gray'>Selected partition:</span> $SELECTED_PARTITION
<span font='11' foreground='gray'>Selected system:</span> $SELECTED_OS
<span font='11' foreground='gray'>Partition format:</span> $PARTITION_FORMAT
<span font='11' foreground='gray'>Partition UUID:</span> $UUID_PARTITION

Do you want to proceed? <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one EFI partition, select which one should use: <span font='16' foreground='LightSkyBlue'>Select an option:</span>

I found more than one Linux installed on this computer, select which one should restore the boot: <span font='16' foreground='LightSkyBlue'>Select an option:</span>
    
I found more than one storage device, select which one should record the boot: <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways (<span foreground='gray'>Simple, Intermediate or Complete</span>),if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. <span font='16' foreground='LightSkyBlue'>Select the desired option:</span>
        
Restoration can be done in 3 ways(<span foreground='gray'>Simple, Intermediate or Complete</span>), if in doubt, try the first
option, restart the computer and see if you resolved.

If the problem persists try the other options. Boot in live mode is using Legacy mode, also called BIOS,
<span foreground='red'>but I found at least one EFI partition on this computer</span>,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in EFI mode.

Do you want to try even with little likely to work? Collect useful logs from installed system. Complete, performs the stages of the intermediate restoration, updates the system and checks if the LTS kernel is installed. Description Disk Done! I didn't find any Linux installed on this computer, canceled process.
 Interactive, opens a TUI control center within the selected system. Interactive, opens a terminal within the selected system. Interactive, opens the package manager within the selected system. Intermediate, reinstalls the GRUB package in the partition, generates the configuration and updates Initrd. Name No EFI partition selected, canceled process. No selected partition, canceled process. Option Partition table Restore Boot, Grub, Initrd and others stuff Restore System with Timeshift ( only if Timeshift is configurated ) Restore the Boot, Grub and Initrd  Simple, just record the grub again at the beginning of the disk. Size The boot in live mode is using EFI mode, but <span foreground='red'>I didn't find any EFI partition</span> on this computer,
probably GRUB restoration will not work.

Try to restart and through the computer BIOS make the boot in Legacy or BIOS mode.

Do you want to try even with little likely to work? This utility should only be used in live mode! Timeshift not installed! Wait... Your computer is not connected to the internet, options 2 and 3 need internet to work. Project-Id-Version: manjaro-rescue
Report-Msgid-Bugs-To: 
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: attranslate
 Această unealtă ar trebui folosită în modul live și pentru a restaura boot-ul Manjaro instalat pe HD sau SSD.

Dacă sistemul instalat pornește corect, nu există probleme în boot, deci este mai bine să nu continuați utilizarea acestei unelte.

Probabil funcționează și cu alte distribuții Linux.

Doriți să continuați? Confirmați datele:
Modul de pornire: efi
Partiția efi: $efi_partition
Partiția selectată: $selected_partition
Sistemul selectat: $selected_os
Formatul partiției: $partition_format
UUID-ul partiției: $uuid_partition

Doriți să continuați? Confirmați datele:
Modul de pornire:
moștenire/bios
Discul selectat: $disk_selected
Tabelul de partiții al discului: $disk_table
Mărimea discului: $disk_size
Partiția selectată: $selected_partition
Sistemul selectat: $selected_os
Formatul partiției: $partition_format
UUID-ul partiției: $uuid_partition

Doriți să continuați? selectați o opțiune:
am găsit mai multe partiții efi, selectați care ar trebui să fie utilizată: Selectați o opțiune:
Am găsit mai multe instalații de Linux pe acest computer, selectați care dintre ele ar trebui să restaureze boot-ul: Selectați o opțiune: am găsit mai mult de un dispozitiv de stocare, selectați care dintre acestea ar trebui să înregistreze pornirea. Selectați opțiunea dorită:

Restaurarea poate fi făcută în 3 moduri (simplu, intermediar sau complet), dacă aveți îndoieli, încercați prima opțiune, reporniți computerul și verificați dacă ați rezolvat problema.

Dacă problema persistă, încercați celelalte opțiuni. Selectați opțiunea dorită:
Restaurarea poate fi făcută în 3 moduri (simplu, intermediar sau complet), dacă aveți îndoieli, încercați prima opțiune, reporniți computerul și verificați dacă ați rezolvat problema.

Dacă problema persistă, încercați celelalte opțiuni. Pornirea în modul live se face folosind modul legacy, numit și bios, dar am găsit cel puțin o partiție efi pe acest calculator, probabil restaurarea grub nu va funcționa.

Încearcă să repornești și prin intermediul bios-ului calculatorului să faci pornirea în modul efi.

Vrei să încerci chiar dacă există șanse mici să funcționeze? Colectați jurnale utile din sistemul instalat. Finalizează, efectuează etapele de restaurare intermediară, actualizează sistemul și verifică dacă kernelul lts este instalat. Descriere Disc Gata! Nu am găsit nicio instalare de Linux pe acest computer, am anulat procesul. Interactiv, deschide un centru de control tui în sistemul selectat. Interactiv, deschide un terminal în cadrul sistemului selectat. Interactiv, deschide managerul de pachete în sistemul selectat. Intermediar, reinstalează pachetul grub în partiție, generează configurația și actualizează initrd. Nume Nu a fost selectată o partiție EFI, procesul a fost anulat. Nicio partiție selectată, proces anulat. Opțiune Tabelul de partiții Restabilizați boot, grub, initrd și alte elemente. Restaurați sistemul cu timeshift (doar dacă timeshift este configurat). Restabiliți boot-ul, grub-ul și initrd-ul. Simplu, înregistrează din nou grub la începutul discului. Mărime Boot-ul în modul live folosește modul efi, dar nu am găsit nicio partiție efi pe acest computer, probabil restaurarea grub nu va funcționa.

Încearcă să repornești și prin bios-ul computerului să faci boot-ul în modul legacy sau bios.

Vrei să încerci chiar dacă este puțin probabil să funcționeze? Această utilitate ar trebui să fie folosită doar în modul live! Timeshift nu este instalat! Așteaptă... Calculatorul tău nu este conectat la internet, opțiunile 2 și 3 necesită internet pentru a funcționa. 